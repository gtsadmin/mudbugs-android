package com.mudbugshockey.application;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.wrapper.StandingsWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;


public class ApplicationUtils extends Application {
    private static ApplicationUtils myApplication = null;

    private static Context ctx;
    private static String MUDBUGS = "MUDBUGS";
    public static SharedPreferences sp;
    public static boolean show_dataListView = true;
    public static final String TAG = "API_RESPONSE";
    private static ProgressDialog dialog;

    private List<StandingsWrapper> sections = new ArrayList<>();

    public List<StandingsWrapper> getSections() {
        return sections;
    }

    public void setSections(List<StandingsWrapper> sections) {
        this.sections = sections;
    }

    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public String getImage_seasonId() {
        return image_seasonId;
    }

    public void setImage_seasonId(String image_seasonId) {
        this.image_seasonId = image_seasonId;
    }

    private String seasonId = "", image_seasonId;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        myApplication = new ApplicationUtils();
        ctx = getApplicationContext();
        sp = ctx.getSharedPreferences(MUDBUGS, 0);
    }

    public static void logOutMe() {
        SharedPreferences sp = ctx.getSharedPreferences(
                MUDBUGS, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(ConstantUtils.KEY_USER_ID);
        editor.remove(ConstantUtils.KEY_FIRST_NAME);
        editor.remove(ConstantUtils.KEY_LAST_NAME);
        editor.remove(ConstantUtils.KEY_PHONE_NUMBER);
        editor.remove(ConstantUtils.KEY_PHOTO_URL);
        editor.remove(ConstantUtils.KEY_EMAIL);
        editor.apply();
    }

    private boolean isAppForeground = false;

    public boolean isAppForeground() {
        return isAppForeground;
    }

    public void setIsForeground(boolean flag) {
        this.isAppForeground = flag;
    }

    //volley starts
    private RequestQueue mRequestQueue;

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(ctx);
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
    //volley ends

    public void hideSoftKeyBoard(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            // check if no view has focus:
            View v = activity.getCurrentFocus();
            if (v == null)
                return;

            inputManager.hideSoftInputFromWindow(v.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void spinnerStart(Context context) {
        // dialog = ProgressDialog(context);
        dialog = new ProgressDialog(context);
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.progressdialog);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }

    public static void spinnerStop() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    public static ApplicationUtils getInstance() {
        if (myApplication == null) {
            myApplication = new ApplicationUtils();
        }
        return myApplication;
    }

    public static String getStringPrefs(String type) {

        String e = sp.getString(type, "");
        return e;
    }


    public static void saveStringPrefs(String type, String value) {

        SharedPreferences.Editor editor = sp.edit();

        editor.putString(type, value);

        editor.commit();
    }

    public static boolean getBooleanPrefs(String keyBoolean) {
        SharedPreferences sp = ctx.getSharedPreferences(MUDBUGS, 0);
        return sp.getBoolean(keyBoolean, false);
    }

    public static void saveBooleanPrefs(String keyBoolean, boolean flag) {
        SharedPreferences sp = ctx.getSharedPreferences(MUDBUGS, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(keyBoolean, flag);
        editor.apply();

    }


    public void produceAnimation(View v) {
        Animation alphaDown = new AlphaAnimation(1.0f, 0.3f);
        Animation alphaUp = new AlphaAnimation(0.3f, 1.0f);
        alphaDown.setDuration(1000);
        alphaUp.setDuration(500);
        alphaDown.setFillAfter(true);
        alphaUp.setFillAfter(true);
        v.startAnimation(alphaUp);
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;

    }

    public static String utfString(String str) {
        try {
            return new String(str.getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void showMessage(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();

    }


    public static String menuTitleTxt(String word) {
        String[] words = word.split(" ");
        StringBuilder sb = new StringBuilder();
        if (words[0].length() > 0) {
            sb.append(Character.toUpperCase(words[0].charAt(0)) + words[0].subSequence(1, words[0].length()).toString().toLowerCase());
            for (int i = 1; i < words.length; i++) {
                sb.append(" ");
                sb.append(Character.toUpperCase(words[i].charAt(0)) + words[i].subSequence(1, words[i].length()).toString().toLowerCase());
            }
        }
        String titleCaseValue = sb.toString();
        return titleCaseValue;
    }

    public static boolean isEmailValid(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static void showSnack(String error, View parentView) {
        Snackbar snackbar = Snackbar
                .make(parentView, error, Snackbar.LENGTH_LONG)
                .setAction(ctx.getResources().getString(R.string.close), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });

        snackbar.show();
    }

    public static String getkeyValue_Str(JSONObject jo, String tag) {
        String key_value = "";

        if (jo.has(tag)) {
            try {
                key_value = jo.getString(tag);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return key_value;
    }

    public boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public String formatDate(String time, String inputPattern, String outputPattern) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.getDefault());

        Date date;
        String str = null;

        try {
            // inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public long getMillisTime(String time, String inputPattern) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
        try {
            Date date = inputFormat.parse(time);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getGmtOffsetString(int offsetMillis) {
        int offsetMinutes = offsetMillis / 60000;
        char sign = '+';
        if (offsetMinutes < 0) {
            sign = '-';
            offsetMinutes = -offsetMinutes;
        }
        return String.format("GMT%c%02d:%02d", sign, offsetMinutes / 60, offsetMinutes % 60);
    }

    public static String formatDistanceString(double value) {
        String formated;
        try {
            formated = String.format("%.2f", value);
        } catch (Exception e) {
            formated = "0";
        }

        return formated;

    }

    public static void simplePop(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(message)
                .setPositiveButton(R.string.ok_, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }
}
