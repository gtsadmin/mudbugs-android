package com.mudbugshockey;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.mudbugshockey.activities.ConfirmDialogListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CustomDialog extends Dialog {
    @Bind(R.id.txtDialogTitle)
    com.mudbugshockey.views.TextViewNunitoRegular txtDialogTitle;
    private ConfirmDialogListener listener;
    @Bind(R.id.txtMessage)
    com.mudbugshockey.views.TextViewNunitoLight txtMessage;
    private String title = "", message = "";
    private int confirmWhat = 0;

    public CustomDialog(Context context, String heading, String msg, int confirmFlag, ConfirmDialogListener listener_) {
        super(context, R.style.Theme_Custom);
        this.listener = listener_;
        this.message = msg;
        this.title = heading;
        this.confirmWhat = confirmFlag;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_custom);
        ButterKnife.bind(this);
        txtMessage.setText(message);
        txtDialogTitle.setText(title);
    }


    @OnClick({R.id.cancelBtn, R.id.confirmBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelBtn:
                dismiss();
                listener.onCancelClick(confirmWhat);
                break;
            case R.id.confirmBtn:
                dismiss();
                listener.onConfirmClicked(confirmWhat);
                break;
        }
    }
}
