package com.mudbugshockey;

public interface EnterEmailListener {
   void onEmailEnter(String[] arr);
}
