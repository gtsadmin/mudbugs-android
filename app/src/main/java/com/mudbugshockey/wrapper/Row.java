package com.mudbugshockey.wrapper;

public class Row {
    private String team_code = "";
    private String wins = "";
    private String losses = "";
    private String ot_losses = "";
    private String ot_wins = "";
    private String shootout_losses = "";
    private String regulation_wins = "";
    private String row = "";
    private String points = "";
    private String penalty_minutes = "";
    private String streak = "";
    private String goals_for = "";
    private String goals_against = "";
    private String goals_diff = "";
    private String percentage = "";
    private String games_played = "";
    private String rank = "";
    private String past_10 = "";
    private String name = "";

    public String getTeam_code() {
        return team_code;
    }

    public void setTeam_code(String team_code) {
        this.team_code = team_code;
    }

    public String getWins() {
        return wins;
    }

    public void setWins(String wins) {
        this.wins = wins;
    }

    public String getLosses() {
        return losses;
    }

    public void setLosses(String losses) {
        this.losses = losses;
    }

    public String getOt_losses() {
        return ot_losses;
    }

    public void setOt_losses(String ot_losses) {
        this.ot_losses = ot_losses;
    }

    public String getOt_wins() {
        return ot_wins;
    }

    public void setOt_wins(String ot_wins) {
        this.ot_wins = ot_wins;
    }

    public String getShootout_losses() {
        return shootout_losses;
    }

    public void setShootout_losses(String shootout_losses) {
        this.shootout_losses = shootout_losses;
    }

    public String getRegulation_wins() {
        return regulation_wins;
    }

    public void setRegulation_wins(String regulation_wins) {
        this.regulation_wins = regulation_wins;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPenalty_minutes() {
        return penalty_minutes;
    }

    public void setPenalty_minutes(String penalty_minutes) {
        this.penalty_minutes = penalty_minutes;
    }

    public String getStreak() {
        return streak;
    }

    public void setStreak(String streak) {
        this.streak = streak;
    }

    public String getGoals_for() {
        return goals_for;
    }

    public void setGoals_for(String goals_for) {
        this.goals_for = goals_for;
    }

    public String getGoals_against() {
        return goals_against;
    }

    public void setGoals_against(String goals_against) {
        this.goals_against = goals_against;
    }

    public String getGoals_diff() {
        return goals_diff;
    }

    public void setGoals_diff(String goals_diff) {
        this.goals_diff = goals_diff;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getGames_played() {
        return games_played;
    }

    public void setGames_played(String games_played) {
        this.games_played = games_played;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPast_10() {
        return past_10;
    }

    public void setPast_10(String past_10) {
        this.past_10 = past_10;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
