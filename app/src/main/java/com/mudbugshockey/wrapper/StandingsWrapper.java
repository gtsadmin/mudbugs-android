package com.mudbugshockey.wrapper;


import java.util.ArrayList;
import java.util.List;

public class StandingsWrapper {

    private String titleName = "";
    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public List<Row> data = new ArrayList<>();
}
