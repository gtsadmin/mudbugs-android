package com.mudbugshockey.wrapper;


public class RosterWrapper {

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getTp_jersey_number() {
        return tp_jersey_number;
    }

    public void setTp_jersey_number(String tp_jersey_number) {
        this.tp_jersey_number = tp_jersey_number;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPlayer_image() {
        return player_image;
    }

    public void setPlayer_image(String player_image) {
        this.player_image = player_image;
    }

    private String first_name = "";
    private String last_name = "";
    private String tp_jersey_number = "";
    private String position = "";
    private String player_image = "";

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    private String playerId ="";
}
