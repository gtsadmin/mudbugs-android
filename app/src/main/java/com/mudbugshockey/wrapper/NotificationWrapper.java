package com.mudbugshockey.wrapper;

import com.google.gson.annotations.SerializedName;

public class NotificationWrapper {

    @SerializedName("id")
    public String id;
    @SerializedName("message")
    public String message;
    @SerializedName("date")
    public String date;
    @SerializedName("create_date")
    public String create_date;

}
