package com.mudbugshockey.wrapper;

import com.google.gson.annotations.SerializedName;

public class NewsWrapper {
    @SerializedName("id")
    public String id;
    @SerializedName("type")
    public String type;
    @SerializedName("picture")
    public String picture;
    @SerializedName("full_picture")
    public String full_picture;
    @SerializedName("message")
    public String message;
    @SerializedName("source")
    public String source;

}
