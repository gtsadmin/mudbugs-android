package com.mudbugshockey.wrapper;

/**
 * Created by kalukhan on 1/4/17.
 */

public class HomeHeaderWrapper {

    public String homelogo = "";
    public String visitedlogo = "";

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String venue = "";


    public String getHomelogo() {
        return homelogo;
    }

    public void setHomelogo(String homelogo) {
        this.homelogo = homelogo;
    }

    public String getVisitedlogo() {
        return visitedlogo;
    }

    public void setVisitedlogo(String visitedlogo) {
        this.visitedlogo = visitedlogo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String date = "";
}
