package com.mudbugshockey.wrapper;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NotificationMainWrapper {
    @SerializedName("status")
    public String status;
    public List<NotificationWrapper> notifications = new ArrayList<>();
}
