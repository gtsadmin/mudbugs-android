package com.mudbugshockey.wrapper;

import com.google.gson.annotations.SerializedName;

public class ScheduleWrapper {
    @SerializedName("GameStatusStringLong")
    public String GameStatusStringLong;
    @SerializedName("Date")
    public String Date;
    @SerializedName("GameDate")
    public String GameDate;
    @SerializedName("HomeGoals")
    public String HomeGoals;
    @SerializedName("VisitorGoals")
    public String VisitorGoals;
    @SerializedName("venue_name")
    public String venue_name;
    @SerializedName("HomeID")
    public String HomeID;
    @SerializedName("VisitorID")
    public String VisitorID;
    @SerializedName("SeasonID")
    public String SeasonID;
    @SerializedName("ScheduledFormattedTime")
    public String ScheduledFormattedTime;

    /*
    *  "ID": "1728",
        "SeasonID": "38",
        "game_number": "1",
        "game_letter": "SC",
        "Date": "2016-09-21",
        "GameDate": "Wed, Sep 21",
        "GameDateISO8601": "2016-09-21T10:00:00-05:00",
        "ScheduledTime": "10:00:00",
        "ScheduledFormattedTime": "10:00 am",
        "Timezone": "Canada\/Central",
        "TicketUrl": "",
        "HomeID": "11",
        "HomeCode": "NTE",
        "HomeCity": "Northeast",
        "HomeNickname": "Generals",
        "HomeLongName": "Northeast Generals",
        "HomeGoals": "1",
        "HomeAudioUrl": "",
        "HomeVideoUrl": "",
        "HomeWebcastUrl": "",
        "VisitorID": "24",
        "VisitorCode": "SHV",
        "VisitorCity": "Shreveport",
        "VisitorNickname": "Mudbugs",
        "VisitorLongName": "Shreveport Mudbugs",
        "VisitorGoals": "3",
        "VisitorAudioUrl": "",
        "VisitorVideoUrl": "",
        "VisitorWebcastUrl": "",
        "Period": "3",
        "PeriodNameShort": "3",
        "PeriodNameLong": "3rd",
        "GameClock": "00:00",
        "GameSummaryUrl": "1728",
        "HomeWins": "4",
        "HomeRegulationLosses": "53",
        "HomeOTLosses": "3",
        "HomeShootoutLosses": "0",
        "VisitorWins": "35",
        "VisitorRegulationLosses": "19",
        "VisitorOTLosses": "3",
        "VisitorShootoutLosses": "3",
        "GameStatus": "4",
        "GameStatusString": "Final",
        "GameStatusStringLong": "Final",
        "Ord": "2016-09-21 11:00:00.000000",
        "venue_name": "Schwan SR 2",
        "venue_location": "1700 105th Ave. NE, Blaine, MN",
        "league_name": "North American Hockey League",
        "TimezoneShort": "CDT"*/

}
