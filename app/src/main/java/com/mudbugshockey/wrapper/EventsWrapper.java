package com.mudbugshockey.wrapper;

import com.google.gson.annotations.SerializedName;

public class EventsWrapper {
    @SerializedName("id")
    public String id;
    @SerializedName("name")
    public String name;
    @SerializedName("startDate")
    public String startDate;
    @SerializedName("eventStatus")
    public String eventStatus;
    @SerializedName("ticketPurchaseUrl")
    public String ticketPurchaseUrl;
    @SerializedName("ticketPrice")
    public String ticketPrice;
    public image image;
}
