package com.mudbugshockey.wrapper;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RosterMainWrapper {
    @SerializedName("parameters")
    public parameters parameters;
    public List<RosterWrapper> Roster = new ArrayList<>();
}
