package com.mudbugshockey.webutility;

import android.app.Activity;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;

import org.json.JSONObject;

import java.util.Map;

public class WebTask {
    private String url = "";
    private Map<String, String> params;
    private WebCompleteTask webCompleteTask;
    private int taskCode;
    private Context context;
    private JSONObject object;
    private boolean shouldShowProgress = true;

    public WebTask(Context context, String url, Map<String, String> params, WebCompleteTask webCompleteTask, int taskCode_) {
        this.url = url;
        this.params = params;
        this.webCompleteTask = webCompleteTask;
        this.taskCode = taskCode_;
        this.context = context;
        if (!ApplicationUtils.isConnectingToInternet(context)) {
            ApplicationUtils.showMessage(context, context.getString(R.string.no_internet));
        } else {
            ApplicationUtils.getInstance().hideSoftKeyBoard((Activity) context);
            volleyStringRequest();
        }
    }
    public WebTask(Context context, String url, Map<String, String> params, WebCompleteTask webCompleteTask, boolean shouldShowProgress, int taskCode_) {
        this.url = url;
        this.params = params;
        this.webCompleteTask = webCompleteTask;
        this.taskCode = taskCode_;
        this.context = context;
        this.shouldShowProgress = shouldShowProgress;
        if (!ApplicationUtils.isConnectingToInternet(context)) {
            ApplicationUtils.showMessage(context, context.getString(R.string.no_internet));
        } else {
            ApplicationUtils.getInstance().hideSoftKeyBoard((Activity) context);
            volleyStringRequest();
        }
    }
    //get api
    public WebTask(Context context, String url,  WebCompleteTask webCompleteTask) {
        this.url = url;
        this.webCompleteTask = webCompleteTask;
        this.context = context;
        if (!ApplicationUtils.isConnectingToInternet(context)) {
            ApplicationUtils.showMessage(context, context.getString(R.string.no_internet));
        } else {
            ApplicationUtils.getInstance().hideSoftKeyBoard((Activity) context);
            volleyGetRequest();
        }
    }
    public WebTask(Context context, String url, JSONObject object, WebCompleteTask webCompleteTask, int taskCode_, boolean shouldShowProgress) {
        this.url = url;
        this.object = object;
        this.webCompleteTask = webCompleteTask;
        this.taskCode = taskCode_;
        this.context = context;
        this.shouldShowProgress = shouldShowProgress;
        if (!ApplicationUtils.isConnectingToInternet(context)) {
            ApplicationUtils.showMessage(context, context.getString(R.string.no_internet));
        } else {
            ApplicationUtils.getInstance().hideSoftKeyBoard((Activity) context);
            volleyJsonRequest(url, this.object);
        }

    }

    private void volleyJsonRequest(String url, JSONObject object) {
        if (shouldShowProgress)
            ApplicationUtils.spinnerStart(context);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if (shouldShowProgress)
                                ApplicationUtils.spinnerStop();
                            webCompleteTask.onComplete(response.toString(), taskCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (shouldShowProgress)
                            ApplicationUtils.spinnerStop();
                        webCompleteTask.onTimeOutError();
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            switch (response.statusCode) {
                                case 400:
                                    //  String json  = new String(response.data);
                                    break;
                            }
                            //Additional cases
                        }
                    }
                }
        );
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setShouldCache(false);
        Volley.newRequestQueue(context).add(jsObjRequest);
    }

    private void volleyStringRequest() {
        if (shouldShowProgress)
        ApplicationUtils.spinnerStart(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (shouldShowProgress)
                ApplicationUtils.spinnerStop();
                webCompleteTask.onComplete(response, taskCode);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (shouldShowProgress)
                ApplicationUtils.spinnerStop();
                webCompleteTask.onTimeOutError();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(stringRequest);
    }

    private void volleyGetRequest() {
        if (shouldShowProgress)
            ApplicationUtils.spinnerStart(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (shouldShowProgress)
                    ApplicationUtils.spinnerStop();
                webCompleteTask.onComplete(response, taskCode);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (shouldShowProgress)
                    ApplicationUtils.spinnerStop();
                webCompleteTask.onTimeOutError();
            }
        }) {


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(stringRequest);
    }
}
