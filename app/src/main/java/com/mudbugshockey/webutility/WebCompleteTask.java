package com.mudbugshockey.webutility;

public interface WebCompleteTask {
    void onComplete(String response, int taskcode);
    void onTimeOutError();
}
