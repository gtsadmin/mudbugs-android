package com.mudbugshockey.webutility;

public class WebUrls {
    public static final String BASE_API_URL = "http://www.mudbugsapp.com/index.php/api/";
    //public static final String  BUY_TICKET_URL = "http://www.mudbugshockey.com/tickets";
    public static final String  MERCHANDISE_URL = "https://squareup.com/store/slapshotmerchandise/?t=merchant-fb";
    public static final String  GEORGE_POND_URL = "http://www.georgespond34.com/";
    public static final String  BASE_URL = "https://lscluster.hockeytech.com/feed/index.php?";
    public static final String  IMAGE_BASE_URL = "https://d1sr8oy6k0deq7.cloudfront.net/nahl/logos/";
    public static final String  TICKET_FLY_URL = "http://www.ticketfly.com/api/events/";
    public static final String  TICKETS_URL = "http://www.ticketfly.com/venue/23559-georges-pond-at-hirsch-coliseum/";

}
