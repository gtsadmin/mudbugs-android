package com.mudbugshockey.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.mudbugshockey.R;
import com.mudbugshockey.adapter.ViewPagerAdapter;
import com.mudbugshockey.fragments.GuestScoreFragment;
import com.mudbugshockey.fragments.HostScoreFragment;
import com.mudbugshockey.fragments.PenaltiesGameFragment;
import com.mudbugshockey.fragments.ScoringGameFragment;
import com.mudbugshockey.fragments.ScoringPlayerFragment;
import com.mudbugshockey.fragments.ShotsFragment;
import com.mudbugshockey.views.TextViewNunitoLight;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GameResultActivity extends AbsGenericActivity {
    @Bind(R.id.toolLayout)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.hostLogoImg)
    ImageView hostLogoImg;
    @Bind(R.id.hostScoreTxt)
    TextViewNunitoLight hostScoreTxt;
    @Bind(R.id.matchNameTxt)
    TextViewNunitoLight matchNameTxt;
    @Bind(R.id.guestScoreTxt)
    TextViewNunitoLight guestScoreTxt;
    @Bind(R.id.guestLogoImg)
    ImageView guestLogoImg;
    @Bind(R.id.tabsScoringShots)
    TabLayout tabsScoringShots;
    @Bind(R.id.vpScoringShots)
    ViewPager vpScoringShots;
    @Bind(R.id.tabsDetails)
    TabLayout tabsDetails;
    @Bind(R.id.vpDetails)
    ViewPager vpDetails;
    @Bind(R.id.tabsScoringPenalties)
    TabLayout tabsScoringPenalties;
    @Bind(R.id.vpScoringPenalties)
    ViewPager vpScoringPenalties;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_game_result);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle("");
            Bundle b = getIntent().getExtras();
            if (b != null) {
                toolbarTitle.setText("Game #371");
            }else{
                toolbarTitle.setText("Game #371");
            }
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.back_arrow);
        }
        //ScoringShots work starts
        Fragment[] fragArr = {new ScoringPlayerFragment(), new ShotsFragment()};
        String tabTitles[] = new String[]{"SCORING", "SHOTS"};
        vpScoringShots.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), fragArr, tabTitles));

        final TabLayout.Tab scoring = tabsScoringShots.newTab();
        final TabLayout.Tab shots = tabsScoringShots.newTab();
        scoring.setText(GameResultActivity.this.getResources().getString(R.string.scoring));
        shots.setText(GameResultActivity.this.getResources().getString(R.string.shots));
        tabsScoringShots.addTab(scoring, 0);
        tabsScoringShots.addTab(shots, 1);
        tabsScoringShots.setTabTextColors(ContextCompat.getColorStateList(GameResultActivity.this, R.color.white));
        tabsScoringShots.setSelectedTabIndicatorColor(ContextCompat.getColor(GameResultActivity.this, R.color.white));
        tabsScoringShots.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(vpScoringShots));
        vpScoringShots.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabsScoringShots));

        //ScoringShots work ends

        //Score details work starts
        Fragment[] fragArr2 = {new HostScoreFragment(), new GuestScoreFragment()};
        String tabTitles2[] = new String[]{"AMARILLO BULLS", "SHREVEPORT MUDBUGS"};
        vpDetails.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), fragArr2, tabTitles2));

        final TabLayout.Tab details_host = tabsDetails.newTab();
        final TabLayout.Tab details_guest = tabsDetails.newTab();
        details_host.setText("AMARILLO BULLS");
        details_guest.setText("SHREVEPORT MUDBUGS");
        tabsDetails.addTab(details_host, 0);
        tabsDetails.addTab(details_guest, 1);
        tabsDetails.setTabTextColors(ContextCompat.getColorStateList(GameResultActivity.this, R.color.white));
        tabsDetails.setSelectedTabIndicatorColor(ContextCompat.getColor(GameResultActivity.this, R.color.white));
        tabsDetails.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(vpDetails));
        vpDetails.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabsDetails));
        //Score details work ends

        //Score details work starts
        Fragment[] fragArr3 = {new ScoringGameFragment(), new PenaltiesGameFragment()};
        String tabTitles3[] = new String[]{"SCORING", "PENALTIES"};
        vpScoringPenalties.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), fragArr3, tabTitles3));

        final TabLayout.Tab scoring_game_host = tabsScoringPenalties.newTab();
        final TabLayout.Tab penalties_game = tabsScoringPenalties.newTab();
        scoring_game_host.setText("SCORING");
        penalties_game.setText("PENALTIES");
        tabsScoringPenalties.addTab(scoring_game_host, 0);
        tabsScoringPenalties.addTab(penalties_game, 1);
        tabsScoringPenalties.setTabTextColors(ContextCompat.getColorStateList(GameResultActivity.this, R.color.white));
        tabsScoringPenalties.setSelectedTabIndicatorColor(ContextCompat.getColor(GameResultActivity.this, R.color.white));
        tabsScoringPenalties.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(vpScoringPenalties));
        vpScoringPenalties.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabsScoringPenalties));
        //Score details work ends
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
