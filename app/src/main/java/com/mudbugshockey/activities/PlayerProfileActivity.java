package com.mudbugshockey.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.views.TextViewNunitoBold;
import com.mudbugshockey.views.TextViewNunitoLight;
import com.mudbugshockey.views.TextViewNunitoRegular;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PlayerProfileActivity extends AbsGenericActivity {
    @Bind(R.id.toolLayout)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.profileImg)
    ImageView profileImg;
    @Bind(R.id.player_name_txt)
    TextViewNunitoRegular playerNameTxt;
    @Bind(R.id.jersey_no_txt)
    TextViewNunitoRegular jerseyNoTxt;
    @Bind(R.id.position_txt)
    TextViewNunitoLight positionTxt;
    @Bind(R.id.lbs_txt)
    TextViewNunitoLight lbsTxt;
    @Bind(R.id.bdate_txt)
    TextViewNunitoLight bdateTxt;
    @Bind(R.id.hometown_txt)
    TextViewNunitoLight hometownTxt;
    @Bind(R.id.regular_season_txt)
    TextViewNunitoBold regularSeasonTxt;
    @Bind(R.id.gp_txt)
    TextViewNunitoLight gpTxt;
    @Bind(R.id.g_txt)
    TextViewNunitoLight gTxt;
    @Bind(R.id.a_txt)
    TextViewNunitoLight aTxt;
    @Bind(R.id.pts_txt)
    TextViewNunitoLight ptsTxt;
    @Bind(R.id.pt_txt)
    TextViewNunitoLight ptTxt;
    @Bind(R.id.playoffs_txt)
    TextViewNunitoBold playoffsTxt;
    @Bind(R.id.gpPlayoffs_txt)
    TextViewNunitoLight gpPlayoffsTxt;
    @Bind(R.id.gPlayoffs_txt)
    TextViewNunitoLight gPlayoffsTxt;
    @Bind(R.id.aPlayoffs_txt)
    TextViewNunitoLight aPlayoffsTxt;
    @Bind(R.id.ptsPlayoffs_txt)
    TextViewNunitoLight ptsPlayoffsTxt;
    @Bind(R.id.ptPlayoffs_txt)
    TextViewNunitoLight ptPlayoffsTxt;

    @Bind(R.id.gpPlayoffTagTxt)
    TextViewNunitoLight gpPlayoffTagTxt;
    @Bind(R.id.gPlayoffTagTxt)
    TextViewNunitoLight gPlayoffTagTxt;
    @Bind(R.id.aPlayoffTagTxt)
    TextViewNunitoLight aPlayoffTagTxt;
    @Bind(R.id.ptsPlayoffTagTxt)
    TextViewNunitoLight ptsPlayoffTagTxt;
    @Bind(R.id.ptPlayoffTagTxt)
    TextViewNunitoLight ptPlayoffTagTxt;

    @Bind(R.id.gpTagTxt)
    TextViewNunitoLight gpTagTxt;
    @Bind(R.id.gTagTxt)
    TextViewNunitoLight gTagTxt;
    @Bind(R.id.aTagTxt)
    TextViewNunitoLight aTagTxt;
    @Bind(R.id.ptsTagTxt)
    TextViewNunitoLight ptsTagTxt;
    @Bind(R.id.ptgTagTxt)
    TextViewNunitoLight ptgTagTxt;


    @Bind(R.id.gameGTxt)
    TextViewNunitoBold gameGTxt;
    @Bind(R.id.gameATxt)
    TextViewNunitoBold gameATxt;
    @Bind(R.id.gamePtsTxt)
    TextViewNunitoBold gamePtsTxt;
    @Bind(R.id.gameShTxt)
    TextViewNunitoBold gameShTxt;
    @Bind(R.id.gamePimTxt)
    TextViewNunitoBold gamePimTxt;


    @Bind(R.id.tableLl)
    LinearLayout tableLl;
    @Bind(R.id.player_profile_ll)
    LinearLayout player_profile_ll;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_player_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbarTitle.setText(PlayerProfileActivity.this.getResources().getString(R.string.player_profile));
        ActionBar actionbar = getSupportActionBar();
        String playerId = "";
        if (actionbar != null) {
            actionbar.setTitle("");
            Bundle b = getIntent().getExtras();
            if (b != null) {
                playerId = b.getString("playerId");
            }
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.back_arrow);
        }
        getPlayerDetails(playerId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getPlayerDetails(String playerId) {
        String param = "feed=statviewfeed&view=player&key=bb92e4a0d0be791d&site_id=2&client_code=nahl&league_id=1&lang=en&fmt=json&statsType=expanded&season_id=" + ConstantUtils.SEASON_ID + "&player_id=" + playerId;
        Map<String, String> params = new HashMap<>();
        new WebTask(PlayerProfileActivity.this, WebUrls.BASE_URL + param, params, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                try {
                    response = response.replace("(", "");
                    response = response.replace(")", "");
                    JSONObject jo = new JSONObject(response);
                    JSONObject infoObj = jo.getJSONObject("info");
                    String profileImgUrl = ApplicationUtils.getkeyValue_Str(infoObj, "profileImage");
                    if (!TextUtils.isEmpty(profileImgUrl))
                        Picasso.with(PlayerProfileActivity.this).load(profileImgUrl).placeholder(R.mipmap.icon)
                                .error(R.mipmap.icon).into(profileImg);

                    playerNameTxt.setText(ApplicationUtils.getkeyValue_Str(infoObj, "firstName") + " " + ApplicationUtils.getkeyValue_Str(infoObj, "lastName"));
                    jerseyNoTxt.setText(ApplicationUtils.getkeyValue_Str(infoObj, "jerseyNumber"));
                    //String shoots = Catches
                    String position = ApplicationUtils.getkeyValue_Str(infoObj, "position");
                    String shoots, shootsValue;
                    if (position.equalsIgnoreCase("G")) {
                        shoots = "Catches";
                        shootsValue = "catches";
                    } else {
                        shoots = "Shoots";
                        shootsValue = "shoots";
                    }
                    positionTxt.setText("Position: " + position + ", "+shoots + " : " + ApplicationUtils.getkeyValue_Str(infoObj, shootsValue));
                    String height = ApplicationUtils.getkeyValue_Str(infoObj, "height");
                    if (height.contains("'")) {
                        String[] spAr = height.split("'");
                        height = spAr[0] + "'" + spAr[1] + "\"";
                    } else
                        height = height + "'0\"";
                    lbsTxt.setText(height + " / " + ApplicationUtils.getkeyValue_Str(infoObj, "weight") + " lbs");
                    bdateTxt.setText("Birthdate: " + ApplicationUtils.getkeyValue_Str(infoObj, "birthDate"));
                    hometownTxt.setText("Hometown: " + ApplicationUtils.getkeyValue_Str(infoObj, "hometown"));
                    JSONArray currentSeasonStats = jo.getJSONArray("currentSeasonStats");
                    for (int i = 0; i < currentSeasonStats.length(); i++) {
                        JSONObject joStats = currentSeasonStats.getJSONObject(i);
                        JSONArray jaSection = joStats.getJSONArray("sections");
                        for (int j = 0; j < jaSection.length(); j++) {
                            JSONObject joSection = jaSection.getJSONObject(j);
                            JSONArray jaData = joSection.getJSONArray("data");
                            for (int k = 0; k < jaData.length(); k++) {
                                JSONObject joData = jaData.getJSONObject(k);
                                JSONObject raw = joData.getJSONObject("row");
                                String season = ApplicationUtils.getkeyValue_Str(raw, "season_name");
                                regularSeasonTxt.setText(season);
                                if (position.equalsIgnoreCase("G")) {
                                    gpTagTxt.setText("W");
                                    gpTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "wins"));
                                    gTagTxt.setText("L");
                                    gTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "losses"));
                                    aTagTxt.setText("SO");
                                    aTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "shutouts"));
                                    ptsTagTxt.setText("GAA");
                                    ptsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "gaa"));
                                    ptgTagTxt.setText("SV%");
                                    ptTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "savepct"));
                                } else {
                                    gpTagTxt.setText("GP");
                                    gpTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "games_played"));
                                    gTagTxt.setText("G");
                                    gTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "goals"));
                                    aTagTxt.setText("A");
                                    aTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "assists"));
                                    ptsTagTxt.setText("PTS");
                                    ptsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "points"));
                                    ptgTagTxt.setText("PT/G");
                                    ptTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "points_per_game"));
                                }
                            }
                        }
                    }

                    JSONArray careerStats = jo.getJSONArray("careerStats");
                    for (int i = 0; i < careerStats.length(); i++) {
                        JSONObject joStats = careerStats.getJSONObject(i);
                        JSONArray jaSection = joStats.getJSONArray("sections");
                        for (int j = 0; j < jaSection.length(); j++) {

                            JSONObject joSection = jaSection.getJSONObject(j);
                            String title = ApplicationUtils.getkeyValue_Str(joSection, "title");

                            if (title.equalsIgnoreCase("Playoffs")) {//playoffs

                                JSONArray jaData = joSection.getJSONArray("data");
                                if (jaData.length() > 0) {
                                    for (int k = 0; k < jaData.length(); k++) {
                                        JSONObject joData = jaData.getJSONObject(0);
                                        JSONObject raw = joData.getJSONObject("row");
                                        playoffsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "season_name"));
                                        position = ApplicationUtils.getkeyValue_Str(infoObj, "position");
                                        if (position.equalsIgnoreCase("G")) {
                                            gpPlayoffTagTxt.setText("W");
                                            gpPlayoffsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "wins"));

                                            gPlayoffTagTxt.setText("L");
                                            gPlayoffsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "losses"));

                                            aPlayoffTagTxt.setText("SO");
                                            aPlayoffsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "shutouts"));

                                            ptsPlayoffTagTxt.setText("GAA");
                                            ptsPlayoffsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "goals_against_average"));

                                            ptPlayoffTagTxt.setText("SV%");
                                            ptPlayoffsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "savepct"));
                                        } else {
                                            gpPlayoffTagTxt.setText("GP");
                                            gpPlayoffsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "games_played"));

                                            gPlayoffTagTxt.setText("G");
                                            gPlayoffsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "goals"));

                                            aPlayoffTagTxt.setText("A");
                                            aPlayoffsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "assists"));

                                            ptsPlayoffTagTxt.setText("PTS");
                                            ptsPlayoffsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "points"));

                                            ptPlayoffTagTxt.setText("PT/G");
                                            ptPlayoffsTxt.setText(ApplicationUtils.getkeyValue_Str(raw, "points_per_game"));
                                        }
                                    }
                                } else {
                                    playoffsTxt.setText("NAHL 2016-2017 Playoffs");
                                    if (ApplicationUtils.getkeyValue_Str(infoObj, "position").equalsIgnoreCase("G")) {
                                        gpPlayoffTagTxt.setText("W");
                                        gpPlayoffsTxt.setText("-");

                                        gPlayoffTagTxt.setText("L");
                                        gPlayoffsTxt.setText("-");

                                        aPlayoffTagTxt.setText("SO");
                                        aPlayoffsTxt.setText("-");

                                        ptsPlayoffTagTxt.setText("GAA");
                                        ptsPlayoffsTxt.setText("-");

                                        ptPlayoffTagTxt.setText("SV%");
                                        ptPlayoffsTxt.setText("-");
                                    } else {
                                        gpPlayoffTagTxt.setText("GP");
                                        gpPlayoffsTxt.setText("-");

                                        gPlayoffTagTxt.setText("G");
                                        gPlayoffsTxt.setText("-");

                                        aPlayoffTagTxt.setText("A");
                                        aPlayoffsTxt.setText("-");

                                        ptsPlayoffTagTxt.setText("PTS");
                                        ptsPlayoffsTxt.setText("-");

                                        ptPlayoffTagTxt.setText("PT/G");
                                        ptPlayoffsTxt.setText("-");
                                    }
                                }
                            }
                        }
                    }
                    LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    JSONArray gameByGameArr = jo.getJSONArray("gameByGame");
                    for (int i = 0; i < gameByGameArr.length(); i++) {
                        JSONObject joGame = gameByGameArr.getJSONObject(i);
                        JSONArray jaSections = joGame.getJSONArray("sections");
                        for (int j = 0; j < jaSections.length(); j++) {
                            JSONObject joSections = jaSections.getJSONObject(j);
                            JSONArray data = joSections.getJSONArray("data");
                            for (int k = 0; k < data.length(); k++) {
                                JSONObject dataObj = data.getJSONObject(k);
                                JSONObject row = dataObj.getJSONObject("row");
                                String game = ApplicationUtils.getkeyValue_Str(row, "game");
                                String date_played = ApplicationUtils.getkeyValue_Str(row, "date_played");
                                String goals, assists, points, shots, penalty_minutes;
                                if (position.equalsIgnoreCase("G")) {
                                    gameGTxt.setText("W");
                                    gameATxt.setText("L");
                                    gamePtsTxt.setText("T");
                                    gameShTxt.setText("OTL");
                                    gamePimTxt.setText("SOL");
                                    goals = ApplicationUtils.getkeyValue_Str(row, "win");
                                    assists = ApplicationUtils.getkeyValue_Str(row, "loss");
                                    points = ApplicationUtils.getkeyValue_Str(row, "tie");
                                    shots = ApplicationUtils.getkeyValue_Str(row, "ot_loss");
                                    penalty_minutes = ApplicationUtils.getkeyValue_Str(row, "shootout_loss");
                                } else {
                                    gameGTxt.setText("G");
                                    gameATxt.setText("A");
                                    gamePtsTxt.setText("PTS");
                                    gameShTxt.setText("SH");
                                    gamePimTxt.setText("PIM");
                                    goals = ApplicationUtils.getkeyValue_Str(row, "goals");
                                    assists = ApplicationUtils.getkeyValue_Str(row, "assists");
                                    points = ApplicationUtils.getkeyValue_Str(row, "points");
                                    shots = ApplicationUtils.getkeyValue_Str(row, "shots");
                                    penalty_minutes = ApplicationUtils.getkeyValue_Str(row, "penalty_minutes");
                                }
                                LinearLayout llSmall = new LinearLayout(PlayerProfileActivity.this);
                                llSmall.setId((k + 1));
                                llSmall.setOrientation(LinearLayout.HORIZONTAL);
                                llp.gravity = Gravity.CENTER;
                                llSmall.setLayoutParams(llp);

                                LinearLayout.LayoutParams llpGame = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.65f);
                                llpGame.gravity = Gravity.CENTER;
                                TextView txtGame = new TextView(PlayerProfileActivity.this);
                                txtGame.setText(game);
                                txtGame.setTextColor(ContextCompat.getColor(PlayerProfileActivity.this, R.color.black));
                                txtGame.setTextSize(10);
                                txtGame.setGravity(Gravity.CENTER);
                                txtGame.setLayoutParams(llpGame);
                                llSmall.addView(txtGame);

                                TextView txtDate = new TextView(PlayerProfileActivity.this);
                                txtDate.setText(date_played);
                                txtDate.setTextColor(ContextCompat.getColor(PlayerProfileActivity.this, R.color.black));
                                txtDate.setTextSize(10);
                                txtDate.setGravity(Gravity.CENTER);
                                txtDate.setLayoutParams(llpGame);
                                llSmall.addView(txtDate);

                                LinearLayout.LayoutParams llpGoals = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.30f);
                                llpGoals.gravity = Gravity.CENTER;
                                TextView txtGoals = new TextView(PlayerProfileActivity.this);
                                txtGoals.setText(goals);
                                txtGoals.setTextColor(ContextCompat.getColor(PlayerProfileActivity.this, R.color.black));
                                txtGoals.setTextSize(12);
                                txtGoals.setGravity(Gravity.CENTER);
                                txtGoals.setLayoutParams(llpGoals);
                                llSmall.addView(txtGoals);

                                TextView txtAssists = new TextView(PlayerProfileActivity.this);
                                txtAssists.setText(assists);
                                txtAssists.setTextColor(ContextCompat.getColor(PlayerProfileActivity.this, R.color.black));
                                txtAssists.setTextSize(12);
                                txtAssists.setGravity(Gravity.CENTER);
                                txtAssists.setLayoutParams(llpGoals);
                                llSmall.addView(txtAssists);

                                LinearLayout.LayoutParams llpPoints = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.45f);
                                llpPoints.gravity = Gravity.CENTER;
                                TextView txtPoints = new TextView(PlayerProfileActivity.this);
                                txtPoints.setText(points);
                                txtPoints.setTextColor(ContextCompat.getColor(PlayerProfileActivity.this, R.color.black));
                                txtPoints.setTextSize(12);
                                txtPoints.setGravity(Gravity.CENTER);
                                txtPoints.setLayoutParams(llpPoints);
                                llSmall.addView(txtPoints);

                                TextView txtShots = new TextView(PlayerProfileActivity.this);
                                txtShots.setText(shots);
                                txtShots.setTextColor(ContextCompat.getColor(PlayerProfileActivity.this, R.color.black));
                                txtShots.setTextSize(12);
                                txtShots.setGravity(Gravity.CENTER);
                                txtShots.setLayoutParams(llpPoints);
                                llSmall.addView(txtShots);

                                TextView txtPenaltyMinutes = new TextView(PlayerProfileActivity.this);
                                txtPenaltyMinutes.setText(penalty_minutes);
                                txtPenaltyMinutes.setTextColor(ContextCompat.getColor(PlayerProfileActivity.this, R.color.black));
                                txtPenaltyMinutes.setTextSize(12);
                                txtPenaltyMinutes.setGravity(Gravity.CENTER);
                                txtPenaltyMinutes.setLayoutParams(llpPoints);
                                llSmall.addView(txtPenaltyMinutes);
                                tableLl.addView(llSmall);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onTimeOutError() {
                try {
                    ApplicationUtils.showSnack(PlayerProfileActivity.this.getResources().getString(R.string.error_internet2),
                            player_profile_ll);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1);
    }
}
