package com.mudbugshockey.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.CustomDialog;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.gcm.MyGcmListenerService;
import com.mudbugshockey.gcm.MyInstanceIDListenerService;
import com.mudbugshockey.gcm.RegistrationIntentService;
import com.mudbugshockey.views.TextViewNunitoRegular;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AbsGenericActivity implements ConfirmDialogListener, WebCompleteTask {
    @Bind(R.id.toolLayout)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.llProfileMain)
    LinearLayout llProfileMain;
    @Bind(R.id.nameTxt)
    TextViewNunitoRegular nameTxt;
    @Bind(R.id.emailTxt)
    TextViewNunitoRegular emailTxt;
    @Bind(R.id.numberTxt)
    TextViewNunitoRegular numberTxt;
    @Bind(R.id.profileImage)
    CircleImageView profileImage;

    private static final int REQUEST_CODE_EDITED = 24;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle("");
            toolbarTitle.setText(getResources().getString(R.string.account_information));
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.back_arrow);
        }
        fillValues();
    }

    private void fillValues() {
        nameTxt.setText(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_FIRST_NAME)+" "
                + ApplicationUtils.getStringPrefs(ConstantUtils.KEY_LAST_NAME));
        emailTxt.setText(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_EMAIL));
        numberTxt.setText(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_PHONE_NUMBER));
        if (!TextUtils.isEmpty(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_PHOTO_URL)))
        Picasso.with(ProfileActivity.this).load(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_PHOTO_URL)).placeholder(R.drawable.icn_profile).into(profileImage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_upload).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_upload:
                Intent in = new Intent(ProfileActivity.this, ProfileEditActivity.class);
                startActivityForResult(in, REQUEST_CODE_EDITED);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logoutDialog() {
        new CustomDialog(ProfileActivity.this, ProfileActivity.this.getResources().getString(
                R.string.logout), ProfileActivity.this.getResources().getString(
                R.string.sure_logout), ConstantUtils.FLAG_LOGOUT, ProfileActivity.this).show();

    }

    private void deleteDialog() {
        new CustomDialog(ProfileActivity.this, ProfileActivity.this.getResources().getString(
                R.string.delete_account), ProfileActivity.this.getResources().getString(
                R.string.sure_delete), ConstantUtils.FLAG_DELETE, ProfileActivity.this).show();

    }

    @Override
    public void onConfirmClicked(int confirmWhat) {
        if (confirmWhat == ConstantUtils.FLAG_LOGOUT) {
            // closeServices();
            setResult(RESULT_OK);
            ApplicationUtils.logOutMe();
            disconnectFromFacebook();
            closeServices();
            startActivity(new Intent(ProfileActivity.this, GetStartedActivity.class));
            finish();
        } else {
            Map<String, String> params = new HashMap<>();
            params.put("logintoken", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_USER_ID));
            new WebTask(ProfileActivity.this, WebUrls.BASE_API_URL + "user/delete", params, ProfileActivity.this,
                    ConstantUtils.TASK_DELETE);
        }
    }

    @Override
    public void onCancelClick(int cancelWhat) {

    }

    @OnClick({R.id.logoutTxt, R.id.deleteAccountTxt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.logoutTxt:
                logoutDialog();
                break;
            case R.id.deleteAccountTxt:
                deleteDialog();
                break;
        }
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.e(ApplicationUtils.TAG, response);
        if (taskcode==ConstantUtils.TASK_DELETE) {
            try {
                JSONObject jo = new JSONObject(response);
                String status = ApplicationUtils.getkeyValue_Str(jo, "status");
                if (status.equals("1")) {
                    setResult(RESULT_OK);
                    ApplicationUtils.logOutMe();
                    disconnectFromFacebook();
                    closeServices();
                    startActivity(new Intent(ProfileActivity.this, GetStartedActivity.class));
                    finish();
                } else {
                    //{"message":"Incorrect Login","status":0,"active":"N"}
                    if (jo.has("message"))
                        ApplicationUtils.showSnack(ApplicationUtils.getkeyValue_Str(jo, "message"), llProfileMain);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTimeOutError() {
        ApplicationUtils.showSnack(ProfileActivity.this.getResources().getString(R.string.error_internet2), llProfileMain);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_EDITED:
                if (resultCode == ConstantUtils.RESULT_UPDATE_PROFILE) {
                    if (data != null)
                        setResult(ConstantUtils.RESULT_UPDATE_PROFILE, data);
                    fillValues();
                }
                break;
        }
    }
    private void closeServices() {
        // Stop services when logging out

        if (isMyServiceRunning(MyGcmListenerService.class)) {
            stopService(new Intent(new Intent(this, MyGcmListenerService.class)));
        }
        if (isMyServiceRunning(MyInstanceIDListenerService.class)) {
            stopService(new Intent(new Intent(this, MyInstanceIDListenerService.class)));
        }
        if (isMyServiceRunning(RegistrationIntentService.class)) {
            stopService(new Intent(new Intent(this, RegistrationIntentService.class)));
        }
    }
    private boolean isMyServiceRunning(Class serviceName) {
        ActivityManager manager = (ActivityManager)
                getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (serviceName.getName().equals(
                    service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    //below function logout user from facebook if login with facebook
    public void disconnectFromFacebook() {
        FacebookSdk.sdkInitialize(ProfileActivity.this);
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }
        LoginManager.getInstance().logOut();
    }
}
