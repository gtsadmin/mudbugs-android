package com.mudbugshockey.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.FileUtils;
import com.mudbugshockey.ImageDialogClickListener;
import com.mudbugshockey.PickImageDialog;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.views.EditTextNunitoRegular;
import com.mudbugshockey.webutility.MultipartRequest;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends AbsGenericActivity {
    @Bind(R.id.toolLayout)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.firstnameEdt)
    EditTextNunitoRegular firstnameEdt;
    @Bind(R.id.lastnameEdt)
    EditTextNunitoRegular lastnameEdt;
    @Bind(R.id.emailEdt)
    EditTextNunitoRegular emailEdt;
    @Bind(R.id.mobilenumberEdt)
    EditTextNunitoRegular mobilenumberEdt;
    @Bind(R.id.passwordEdt)
    EditTextNunitoRegular passwordEdt;
    @Bind(R.id.llRegistration)
    LinearLayout llRegistration;
    @Bind(R.id.profileImage_)
    de.hdodenhof.circleimageview.CircleImageView profileImage_;

    private boolean isEyeOpen = false;
    private File outFile = FileUtils.getOutputMediaFile();
    private String imgPath = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle("");
            toolbarTitle.setText(RegistrationActivity.this.getResources().getString(R.string.registration));
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.back_arrow);
        }
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA},
                    ConstantUtils.MY_PERMISSION_CAMERA);
        }
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    ConstantUtils.PERMISSION_READ_EXTERNAL_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    ConstantUtils.PERMISSION_WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick({R.id.imgPwdVisible, R.id.getstartedTxt, R.id.profileImage_})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgPwdVisible:
                isEyeOpen = !isEyeOpen;
                ImageView imgEye = (ImageView) view;
                if (isEyeOpen) {
                    imgEye.setImageResource(R.drawable.eye);
                    passwordEdt.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    imgEye.setImageResource(R.drawable.closedeye);
                    passwordEdt.setInputType(129);
                }
                break;
            case R.id.getstartedTxt:
                ApplicationUtils.getInstance().hideSoftKeyBoard(RegistrationActivity.this);
                String[] arrCheck = data();
                if (checkInputAvailable(arrCheck)) {
                    if (TextUtils.isEmpty(imgPath)) {
                        signUpTask();
                    } else {
                        signUpWithFile();
                    }
                }
                break;
            case R.id.profileImage_:
                new PickImageDialog(RegistrationActivity.this, new ImageDialogClickListener() {
                    @Override
                    public void imageGalleryClick() {
                        if (checkLocationPermission("android.permission.CAMERA") && checkLocationPermission("android.permission.READ_EXTERNAL_STORAGE") && checkLocationPermission("android.permission.WRITE_EXTERNAL_STORAGE"))
                            if (checkLocationPermission("android.permission.READ_EXTERNAL_STORAGE") && checkLocationPermission("android.permission.WRITE_EXTERNAL_STORAGE"))
                                openGalleryPhotos();
                        // pickFromGallery();
                    }

                    @Override
                    public void imageCameraClick() {
                        if (checkLocationPermission("android.permission.CAMERA") && checkLocationPermission("android.permission.READ_EXTERNAL_STORAGE") && checkLocationPermission("android.permission.WRITE_EXTERNAL_STORAGE"))
                            openCamera();
                        // captureImage();
                    }
                }).show();
                break;
        }
    }

    private void signUpTask() {
        Map<String, String> params = new HashMap<>();
        params.put("first_name", getEditedText(firstnameEdt));
        params.put("last_name", getEditedText(lastnameEdt));
        params.put("email", getEditedText(emailEdt));
        params.put("mobile_no", getEditedText(mobilenumberEdt));
        params.put("password", getEditedText(passwordEdt));
        new WebTask(RegistrationActivity.this, WebUrls.BASE_API_URL + "auth/register", params, new WebCompleteTask() {
            @Override
            public void onTimeOutError() {
                ApplicationUtils.showSnack(RegistrationActivity.this.getResources().getString(R.string.error_internet2), llRegistration);
            }

            @Override
            public void onComplete(String response, int taskcode) {
                Log.e(ApplicationUtils.TAG, response);
                try {
                   // {"user":{"id":"64","name":"Devid Rocco","first_name":"Devid","last_name":"Rocco","email":"devid@gmail.com","phone":"9829671955","image":"http:\/\/www.orphcnul.com\/mudbugs\/uploads\/user\/64_58e63f27cbf45.jpg","thumb_image":"http:\/\/www.orphcnul.com\/mudbugs\/uploads\/user\/thumb\/64_58e63f27cbf45.jpg"},"logintoken":"a3b9330ce40ff17ab54a05d23543d528","message":"Logged In Successfully","status":1,"active":"Y"}
                    JSONObject jo = new JSONObject(response);
                    String status = ApplicationUtils.getkeyValue_Str(jo, "status");

                    if (status.equals("1")) {
                        ApplicationUtils.showMessage(RegistrationActivity.this, RegistrationActivity.this.getResources().getString(R.string.signUp_success));
                        setResult(RESULT_OK);
                        // startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
                        //finish();

                        JSONObject joInfo = jo.getJSONObject("user");
                        ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_USER_ID, ApplicationUtils.getkeyValue_Str(jo, "logintoken"));
                        ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_FIRST_NAME, ApplicationUtils.getkeyValue_Str(joInfo, "first_name"));
                        ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_LAST_NAME, ApplicationUtils.getkeyValue_Str(joInfo, "last_name"));
                        ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHONE_NUMBER, ApplicationUtils.getkeyValue_Str(joInfo, "phone"));
                        ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHOTO_URL, ApplicationUtils.getkeyValue_Str(joInfo, "image"));
                        ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_EMAIL, ApplicationUtils.getkeyValue_Str(joInfo, "email"));
                        setResult(RESULT_OK);
                        startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
                        finish();

                    } else {
                        if (jo.has("message"))
                            ApplicationUtils.simplePop(RegistrationActivity.this,ApplicationUtils.getkeyValue_Str(jo, "message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, ConstantUtils.TASK_UPDATE);
    }

    private void signUpWithFile() {
        Map<String, String> params = new HashMap<>();
        params.put("first_name", getEditedText(firstnameEdt));
        params.put("last_name", getEditedText(lastnameEdt));
        params.put("email", getEditedText(emailEdt));
        params.put("mobile_no", getEditedText(mobilenumberEdt));
        params.put("password", getEditedText(passwordEdt));
        ApplicationUtils.spinnerStart(RegistrationActivity.this);
        MultipartRequest multipartRequest = new MultipartRequest(WebUrls.BASE_API_URL + "auth/register",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ApplicationUtils.spinnerStop();
                        Log.e(ApplicationUtils.TAG, response);
                        //{"message":"Registration successfully and account verification's mail sent to your email id.","status":1,"active":"N"}
                        try {
                            JSONObject jo = new JSONObject(response);
                            String status = ApplicationUtils.getkeyValue_Str(jo, "status");

                            if (status.equals("1")) {
                                ApplicationUtils.showMessage(RegistrationActivity.this, RegistrationActivity.this.getResources().getString(R.string.signUp_success));
                               // setResult(RESULT_OK);
                                //startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
                                //finish();
                                JSONObject joInfo = jo.getJSONObject("user");
                                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_USER_ID, ApplicationUtils.getkeyValue_Str(jo, "logintoken"));
                                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_FIRST_NAME, ApplicationUtils.getkeyValue_Str(joInfo, "first_name"));
                                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_LAST_NAME, ApplicationUtils.getkeyValue_Str(joInfo, "last_name"));
                                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHONE_NUMBER, ApplicationUtils.getkeyValue_Str(joInfo, "phone"));
                                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHOTO_URL, ApplicationUtils.getkeyValue_Str(joInfo, "image"));
                                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_EMAIL, ApplicationUtils.getkeyValue_Str(joInfo, "email"));
                                setResult(RESULT_OK);
                                startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
                                finish();
                            } else {
                                //{"message":"Incorrect Login","status":0,"active":"N"}
                                if (jo.has("message"))
                                    ApplicationUtils.simplePop(RegistrationActivity.this, ApplicationUtils.getkeyValue_Str(jo, "message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ApplicationUtils.spinnerStop();
                Log.d("error", error.toString());
            }
        }, new File(imgPath), "image", params);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ApplicationUtils.getInstance().addToRequestQueue(multipartRequest);
    }

    private String[] data() {
        String[] arr = new String[5];
        arr[0] = getEditedText(firstnameEdt);
        arr[1] = getEditedText(lastnameEdt);
        arr[2] = getEditedText(emailEdt);
        arr[3] = getEditedText(mobilenumberEdt);
        arr[4] = getEditedText(passwordEdt);
        return arr;

    }

    private String getEditedText(EditText edt) {
        String s = edt.getText().toString();
        if (TextUtils.isEmpty(s)) {
            s = "";
        }
        return s;
    }

    private boolean checkInputAvailable(String[] ar) {
        boolean retVal = true;
        String name = ar[0];
        String email = ar[2];
        String phone = ar[3];
        String password = ar[4];

        if (TextUtils.isEmpty(password) || password.length() < 5 || password.contains(" ")) {
            ApplicationUtils.showSnack(RegistrationActivity.this.getResources().getString(R.string.passwordError), llRegistration);
            retVal = false;
        }
        if (TextUtils.isEmpty(phone) || !ApplicationUtils.getInstance().isValidPhoneNumber(phone) || phone.length() < 6 || phone.length() > 13) {
            ApplicationUtils.showSnack(RegistrationActivity.this.getResources().getString(R.string.phoneError), llRegistration);
            retVal = false;
        }
        if (!ApplicationUtils.isValidEmail(email)) {
            ApplicationUtils.showSnack(RegistrationActivity.this.getResources().getString(R.string.emailError), llRegistration);
            retVal = false;
        }
        if (TextUtils.isEmpty(name)) {
            ApplicationUtils.showSnack(RegistrationActivity.this.getResources().getString(R.string.nameEmptyError), llRegistration);
            retVal = false;
        }

        return retVal;
    }


    //permission work
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ConstantUtils.MY_PERMISSION_CAMERA:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //openCamera();

                }// else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                //}
                break;

            case ConstantUtils.PERMISSION_READ_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //openCamera();

                }// else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                //}
                break;
            case ConstantUtils.PERMISSION_WRITE_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //openCamera();

                }// else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                //}
                break;
        }
    }


    public boolean checkLocationPermission(String permission) {
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private void openCamera() {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            ApplicationUtils.showSnack(RegistrationActivity.this.getResources().getString(R.string.no_sd_card), llRegistration);
        } else {
            try {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri outUri = Uri.fromFile(outFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outUri);
                RegistrationActivity.this.startActivityForResult(cameraIntent, ConstantUtils.REQUEST_CODE_CAMERA_PIC);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void openGalleryPhotos() {
        //   Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        RegistrationActivity.this.startActivityForResult(photoPickerIntent, ConstantUtils.REQUEST_CODE_GALLERY_IMAGES);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ConstantUtils.REQUEST_CODE_GALLERY_IMAGES:
                if (resultCode == Activity.RESULT_OK) {

                    try {
                        Uri selectedImage = data.getData();
                      /*   String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = RegistrationActivity.this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        if (cursor != null) {
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imgPath = cursor.getString(columnIndex);
                            cursor.close();
                        }*/
                        imgPath = FileUtils.getPath(RegistrationActivity.this, selectedImage);
                        performCrop(imgPath);
                        //cursor.close();

                        /*
                         * *
                         * kalu     Uri selectedImageUri = data.getData();
                         imgPath = getPath(selectedImageUri);
                         performCrop(imgPath);**/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            case ConstantUtils.REQUEST_CODE_CAMERA_PIC:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        if (outFile.exists()) {
                            imgPath = outFile.getPath();
                            // Picasso.with(RegistrationActivity.this).load(new File(imgPath)).into(circleView);
                            performCrop(imgPath);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            case ConstantUtils.REQUEST_CODE_CROP_PIC:
                if (resultCode == Activity.RESULT_OK) {
                    // get the returned data
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        Bitmap bitmap = extras.getParcelable("data");
                        Bitmap[] bitArr = {bitmap};
                        new SaveCroppedTask().execute(bitArr);
                    } else {
                        Picasso.with(RegistrationActivity.this).load(new File(imgPath)).placeholder(R.drawable.icn_profile).into(profileImage_);
                    }
                }
                break;

            default:
                break;
        }
    }

    private void performCrop(String mCurrentPhotoPath) {
        try {
            // Initialize intent
            Intent intent = new Intent("com.android.camera.action.CROP");
// set data type to be sent
            intent.setType("image/*");
// get croppers available in the phone
            List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
            int size = list.size();
// handle the case if there's no cropper in the phone
            if (size == 0) {
                ApplicationUtils.showSnack("Can not find image crop app", llRegistration);
                return;
            } else {
// now this is the case if cropper exists
// initialize the Uri for the captures or gallery image
                Uri imageUri = Uri.fromFile(new File(mCurrentPhotoPath));
                // Send the Uri path to the cropper intent
                intent.setData(imageUri);
                intent.putExtra("outputX", 200);
                intent.putExtra("outputY", 200);
                intent.putExtra("aspectX", 4);
                intent.putExtra("aspectY", 4);
                intent.putExtra("scale", true);
                // Here's my attempt to ask the intent to save output data as file
                //  File f = null;
                // Here I initialize empty file
                // This returns the file created
                // f = outFile;
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outFile));
            }
            // --------------------------------------------------------------------
            // -----------> When changing this to false it worked <----------------
            // --------------------------------------------------------------------
            intent.putExtra("return-data", true);
            // --------------------------------------------------------------------
            // --------------------------------------------------------------------
            // If there's only 1 Cropper in the phone (e.g. Gallery )
            if (size > 0) {
                // get the cropper intent found
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);
                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                startActivityForResult(i, ConstantUtils.REQUEST_CODE_CROP_PIC);
            }
        } catch (ActivityNotFoundException e) {
            ApplicationUtils.showSnack("Device doesn't support the crop action", llRegistration);
        }
    }

    //below task saves bitmap as file on sd-card
    private class SaveCroppedTask extends AsyncTask<Bitmap, Void, String> {

        @Override
        protected String doInBackground(Bitmap... params) {
            String path;
            if (null != params[0]) {
                path = FileUtils.saveImage(RegistrationActivity.this, params[0]);
            } else {
                path = imgPath;
            }
            return path;
        }

        @Override
        protected void onPostExecute(String path) {
            super.onPostExecute(path);
            System.out.println("savedBitmap>> " + path);
            imgPath = path;
            Picasso.with(RegistrationActivity.this).load(new File(imgPath)).placeholder(R.drawable.icn_profile).into(profileImage_);
        }


    }

}
