package com.mudbugshockey.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NotificationActivity extends AbsGenericActivity {
    @Bind(R.id.toolLayout)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.teamNewsSwitch)
    Switch teamNewsSwitch;
    @Bind(R.id.gameStartSwitch)
    Switch gameStartSwitch;
    @Bind(R.id.gameResultSwitch)
    Switch gameResultSwitch;
    @Bind(R.id.goalsSwitch)
    Switch goalsSwitch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle("");
            toolbarTitle.setText(NotificationActivity.this.getResources().getString(R.string.notification));
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.back_arrow);
        }

        teamNewsSwitch.setChecked(ApplicationUtils.getBooleanPrefs(ConstantUtils.KEY_NEWS_FLAG));
        gameStartSwitch.setChecked(ApplicationUtils.getBooleanPrefs(ConstantUtils.KEY_GAME_START));
        gameResultSwitch.setChecked(ApplicationUtils.getBooleanPrefs(ConstantUtils.KEY_GAME_RESULT));
        goalsSwitch.setChecked(ApplicationUtils.getBooleanPrefs(ConstantUtils.KEY_GOALS));

        teamNewsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ApplicationUtils.saveBooleanPrefs(ConstantUtils.KEY_NEWS_FLAG, isChecked);
            }
        });

        gameStartSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ApplicationUtils.saveBooleanPrefs(ConstantUtils.KEY_GAME_START, isChecked);
            }
        });

        gameResultSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ApplicationUtils.saveBooleanPrefs(ConstantUtils.KEY_GAME_RESULT, isChecked);
            }
        });

        goalsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ApplicationUtils.saveBooleanPrefs(ConstantUtils.KEY_GOALS, isChecked);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
