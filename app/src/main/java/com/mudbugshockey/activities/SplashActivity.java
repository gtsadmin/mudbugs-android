package com.mudbugshockey.activities;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Window;

import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;

import butterknife.ButterKnife;


public class SplashActivity extends AbsGenericActivity {
    private static final int SPLASH_TIME = 2;//sec
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        Handler handler = new Handler();
        Runnable r = new Runnable() {

            @Override
            public void run() {
               if(TextUtils.isEmpty(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_USER_ID))){
                    switchActivity(SplashActivity.this, GetStartedActivity.class);
                    finish();
                }else{
                    switchActivity(SplashActivity.this, MainActivity.class);
                    finish();
                }
            }
        };
        handler.postDelayed(r, SPLASH_TIME*1000);

    }




}
