package com.mudbugshockey.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.EnterEmailDialog;
import com.mudbugshockey.EnterEmailListener;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.gcm.RegistrationIntentService;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GetStartedActivity extends AbsGenericActivity {

    @Bind(R.id.rlGetStarted)
    RelativeLayout rlGetStarted;
    CallbackManager callbackManager;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);
        ButterKnife.bind(this);
        FacebookSdk.sdkInitialize(GetStartedActivity.this);
/*
* *facebook login*/
        callbackManager = CallbackManager.Factory.create();
        //new method
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject joGraph, GraphResponse response) {
                                        Log.v("LoginActivityFB", response.toString());
                                        // Application code
                                        try {
                                            //{Response:  responseCode: 200, graphObject: {"id":"719107258270360","name":"SB Logic","email":"logicsnano@gmail.com","gender":"male"}, error: null}
                                            if (TextUtils.isEmpty(joGraph.getString("email"))) {
                                                String fbProfileImage = "https://graph.facebook.com/" + joGraph.getString("id") + "/picture?type=large";
                                                String[] arr = {joGraph.getString("name"), joGraph.getString("id"), fbProfileImage, ""};
                                                new EnterEmailDialog(GetStartedActivity.this, arr, new EnterEmailListener() {
                                                    @Override
                                                    public void onEmailEnter(String[] arrVal) {

                                                        Map<String, String> params = new HashMap<>();
                                                        params.put("type", "F");
                                                        params.put("first_name", arrVal[0]);
                                                        params.put("last_name", "");
                                                        params.put("fb_code", arrVal[1]);
                                                        params.put("fb_image", arrVal[2]);
                                                        params.put("email", arrVal[3]);
                                                        params.put("device_token", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_DEVICE_TOKEN));
                                                        params.put("device_type", "A");
                                                        loginSocial(params);
                                                    }
                                                }).show();
                                            } else {
                                                String fbProfileImage = "https://graph.facebook.com/" + joGraph.getString("id") + "/picture?type=large";
                                                Map<String, String> params = new HashMap<>();
                                                params.put("type", "F");
                                                params.put("first_name", joGraph.getString("name"));
                                                params.put("last_name", "");
                                                params.put("fb_code", joGraph.getString("id"));
                                                params.put("fb_image", fbProfileImage);
                                                params.put("email", joGraph.getString("email"));
                                                params.put("device_token", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_DEVICE_TOKEN));
                                                params.put("device_type", "A");
                                                loginSocial(params);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        ApplicationUtils.showSnack("Login Cancel", rlGetStarted);
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        ApplicationUtils.showSnack(exception.getMessage(), rlGetStarted);
                    }
                });


        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ConstantUtils.RESULT_FINISH_ACTIVITY:
                if (resultCode == RESULT_OK)
                    finish();
                break;
            default:
                callbackManager.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }

    @OnClick({R.id.signInTxt, R.id.fbTxt, R.id.notMemberTxt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signInTxt:
                startActivityForResult(new Intent(GetStartedActivity.this, LoginActivity.class), ConstantUtils.RESULT_FINISH_ACTIVITY);
                break;
            case R.id.fbTxt:
                ApplicationUtils.getInstance().produceAnimation(view);
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
                break;
            case R.id.notMemberTxt:
                ApplicationUtils.getInstance().produceAnimation(view);
                startActivityForResult(new Intent(GetStartedActivity.this, RegistrationActivity.class), ConstantUtils.RESULT_FINISH_ACTIVITY);

                break;
        }


    }

    private void loginSocial(Map map) {
        new WebTask(GetStartedActivity.this, WebUrls.BASE_API_URL + "auth/register", map, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                try {
                    JSONObject jo = new JSONObject(response);
                    String status = ApplicationUtils.getkeyValue_Str(jo, "status");
                    if (status.equals("1")) {
                        //    {"user":{"id":"12","name":null,"first_name":"Kalu Khan","last_name":null,"email":"khanrocks88@gmail.com","phone":null,"image":"https:\/\/graph.facebook.com\/714688345254669\/picture?type=large","thumb_image":"https:\/\/graph.facebook.com\/714688345254669\/picture?type=large"},"logintoken":"1522e2ccd5776df6f11d773f19f9adfa","message":"Logged In Successfully","status":1,"active":"Y"}
                        JSONObject joInfo = jo.getJSONObject("user");
                        ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_USER_ID, ApplicationUtils.getkeyValue_Str(jo, "logintoken"));
                        ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_FIRST_NAME, ApplicationUtils.getkeyValue_Str(joInfo, "first_name"));
                        String lastName = ApplicationUtils.getkeyValue_Str(joInfo, "last_name");
                        if (!lastName.equals("null"))
                            ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_LAST_NAME, lastName);
                        String phone = ApplicationUtils.getkeyValue_Str(joInfo, "phone");
                        if (!phone.equals("null"))
                            ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHONE_NUMBER, phone);
                        ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHOTO_URL, ApplicationUtils.getkeyValue_Str(joInfo, "image"));
                        ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_EMAIL, ApplicationUtils.getkeyValue_Str(joInfo, "email"));
                        setResult(RESULT_OK);
                        startActivity(new Intent(GetStartedActivity.this, MainActivity.class));
                        finish();
                    } else {
                        //{"message":"Incorrect Login","status":0,"active":"N"}
                        if (jo.has("message"))
                            ApplicationUtils.showSnack(ApplicationUtils.getkeyValue_Str(jo, "message"), rlGetStarted);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTimeOutError() {
                ApplicationUtils.showSnack(GetStartedActivity.this.getResources().getString(R.string.error_internet2), rlGetStarted);
            }
        }, ConstantUtils.TASK_DEFAULT);
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(ApplicationUtils.TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

}
