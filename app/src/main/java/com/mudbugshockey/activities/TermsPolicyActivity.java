package com.mudbugshockey.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TermsPolicyActivity extends AbsGenericActivity {
    @Bind(R.id.toolLayout)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.linearMainTerms)
    LinearLayout linearMainTerms;
    @Bind(R.id.llWebView)
    LinearLayout llWebView;
    private WebView webView;
    private String tag = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_policy);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        Bundle bun = getIntent().getExtras();
        if (null != bun) {
            tag = bun.getString("tag");
        }
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle("");
            if (tag.equals("Terms"))
                toolbarTitle.setText(TermsPolicyActivity.this.getResources().getString(R.string.terms_of_service));
            else
                toolbarTitle.setText(TermsPolicyActivity.this.getResources().getString(R.string.privacy_policy));

            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.back_arrow);
        }


        webView = new WebView(TermsPolicyActivity.this);
        webView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
//		webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        //webView.setBackgroundColor(ContextCompat.getColor(TermsPolicyActivity.this, R.color.grey));
        getTextServer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //server action
    private void getTextServer() {
        Map<String, String> params = new HashMap<>();
        params.put("logintoken", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_USER_ID));
        new WebTask(TermsPolicyActivity.this, WebUrls.BASE_API_URL + "user/pages", params, new WebCompleteTask() {
            @Override
            public void onTimeOutError() {
                ApplicationUtils.showSnack(TermsPolicyActivity.this.getResources().getString(R.string.error_internet2), linearMainTerms);
            }

            @Override
            public void onComplete(String response, int taskcode) {
                try {

                    JSONObject jo = new JSONObject(response);
                    String status = ApplicationUtils.getkeyValue_Str(jo, "status");
                    if (status.equals("1")) {
                        JSONArray ja = jo.getJSONArray("pages");
                        String text = "";
                        for (int i = 0; i < ja.length(); i++) {
                            JSONObject jObj = ja.getJSONObject(i);
                            String title = ApplicationUtils.getkeyValue_Str(jObj, "title");
                            if (tag.equals("Terms")) {
                                if (title.equals("Terms & Conditions"))
                                    text = ApplicationUtils.getkeyValue_Str(jObj, "description");
                            } else if (tag.equals("Privacy")) {
                                if (title.equals("Privacy Policy"))
                                    text = ApplicationUtils.getkeyValue_Str(jObj, "description");
                            }

                        }
                        if (webView != null)
                            llWebView.removeView(webView);
                        llWebView.addView(webView);
                        String mimeType = "text/html";
                        String encoding = "UTF-8";
                        if (!TextUtils.isEmpty(text))
                            webView.loadDataWithBaseURL("", getHtmlData(text), mimeType, encoding, "");//
                    } else {
                        if (jo.has("message"))
                            ApplicationUtils.showSnack(ApplicationUtils.getkeyValue_Str(jo, "message"), linearMainTerms);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, ConstantUtils.TASK_DEFAULT);
    }

    private String getHtmlData(String data) {//27
        String head = "<style>body{margin:0px 15px}body,table{font-family:'SonySketchEF';font-size:27pt;background:#ffffff;color:black;}strong{display:block;font-size:1.2em;text-transform:uppercase;text-align:left}</style>";
        String htmlData = "<html>" + head + "<body>" + data + "</body></html>";
        return htmlData;
    }
}
