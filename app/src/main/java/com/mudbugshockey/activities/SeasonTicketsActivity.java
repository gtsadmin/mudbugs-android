package com.mudbugshockey.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.adapter.TicketFlyRclAdapter;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.mudbugshockey.wrapper.TicketMainWrapper;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SeasonTicketsActivity extends AbsGenericActivity {
    @Bind(R.id.toolLayout)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;

    @Bind(R.id.rclSchedule)
    RecyclerView rclSchedule;
    @Bind(R.id.swpLayoutSchedule)
    android.support.v4.widget.SwipeRefreshLayout swpLayoutSchedule;
    @Bind(R.id.llSchedule)
    LinearLayout llSchedule;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_season_tickets);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle("");
            toolbarTitle.setText(SeasonTicketsActivity.this.getResources().getString(R.string.season_tickets));
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.back_arrow);
        }

        // Creating a layout Manager
        mLayoutManager = new LinearLayoutManager(SeasonTicketsActivity.this);
        rclSchedule.setLayoutManager(mLayoutManager);

        getData();
        swpLayoutSchedule.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void getData() {
        String param = "upcoming.json?orgId=6847&fields=id,name,startDate,ticketPurchaseUrl,eventStatus,ticketPrice,image";
        Map<String, String> params = new HashMap<>();
        new WebTask(SeasonTicketsActivity.this, WebUrls.TICKET_FLY_URL + param, params, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                cancelRefreshing();
                try {
                    Gson gson = new Gson();
                    TicketMainWrapper wrapper = gson.fromJson(response, TicketMainWrapper.class);
                    TicketFlyRclAdapter adapter = new TicketFlyRclAdapter(SeasonTicketsActivity.this,
                            wrapper.events, new RecycleClickListener() {
                        @Override
                        public void onRecycleClick(int position) {
                            // startActivity(new Intent(getActivity(), GameResultActivity.class));
                        }
                    });

                    rclSchedule.setAdapter(adapter);// Setting the adapter to RecyclerView
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onTimeOutError() {
                try {
                    cancelRefreshing();
                    ApplicationUtils.showSnack(SeasonTicketsActivity.this.getResources().getString(R.string.error_internet2),
                            llSchedule);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1);
    }

    private void cancelRefreshing() {
        if (swpLayoutSchedule != null)
            swpLayoutSchedule.setRefreshing(false);
    }

}
