package com.mudbugshockey.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.FileUtils;
import com.mudbugshockey.ImageDialogClickListener;
import com.mudbugshockey.PickImageDialog;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.views.EditTextNunitoRegular;
import com.mudbugshockey.webutility.MultipartRequest;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileEditActivity extends AbsGenericActivity {
    @Bind(R.id.toolLayout)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.llEditProfileMain)
    LinearLayout llEditProfileMain;
    @Bind(R.id.profileEditImage)
    CircleImageView profileEditImage;
    @Bind(R.id.firstNameEditEdt)
    EditTextNunitoRegular firstNameEditEdt;
    @Bind(R.id.lastNameEditEdt)
    EditTextNunitoRegular lastNameEditEdt;
    @Bind(R.id.emailEditEdt)
    EditTextNunitoRegular emailEditEdt;
    @Bind(R.id.mobileNumberEditEdt)
    EditTextNunitoRegular mobileNumberEditEdt;


    private File outFile = FileUtils.getOutputMediaFile();
    private String imgPath = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle("");
            toolbarTitle.setText(getResources().getString(R.string.account_information));
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.back_arrow);
        }
        emailEditEdt.setFocusable(false);

        if (ContextCompat.checkSelfPermission(ProfileEditActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ProfileEditActivity.this, new String[]{android.Manifest.permission.CAMERA},
                    ConstantUtils.MY_PERMISSION_CAMERA);
        }
        if (ContextCompat.checkSelfPermission(ProfileEditActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ProfileEditActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    ConstantUtils.PERMISSION_READ_EXTERNAL_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(ProfileEditActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ProfileEditActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    ConstantUtils.PERMISSION_WRITE_EXTERNAL_STORAGE);
        }
        String phoneNo = ApplicationUtils.getStringPrefs(ConstantUtils.KEY_PHONE_NUMBER);
        String substring = phoneNo.length() > 10 ? phoneNo.substring(phoneNo.length() - 10) : phoneNo;
        firstNameEditEdt.setText(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_FIRST_NAME));
        lastNameEditEdt.setText(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_LAST_NAME));
        mobileNumberEditEdt.setText(substring);
        emailEditEdt.setText(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_EMAIL));
        if (!TextUtils.isEmpty(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_PHOTO_URL)))
            Picasso.with(ProfileEditActivity.this).load(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_PHOTO_URL)).placeholder(R.drawable.icn_profile).into(profileEditImage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_upload).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private String[] data() {
        String[] arr = new String[3];
        arr[0] = getEditedText(firstNameEditEdt);
        arr[1] = getEditedText(lastNameEditEdt);
        arr[2] = getEditedText(mobileNumberEditEdt);

        return arr;

    }

    private String getEditedText(EditText edt) {
        String s = edt.getText().toString();
        if (TextUtils.isEmpty(s)) {
            s = "";
        }
        return s;
    }

    private boolean checkInputAvailable(String[] ar) {
        boolean retVal = true;
        String name = ar[0];
        String phone = ar[2];
        if (TextUtils.isEmpty(phone) || !ApplicationUtils.getInstance().isValidPhoneNumber(phone) || phone.length() < 6 || phone.length() > 13) {
            ApplicationUtils.showSnack(ProfileEditActivity.this.getResources().getString(R.string.phoneError), llEditProfileMain);
            retVal = false;
        }
        if (TextUtils.isEmpty(name)) {
            ApplicationUtils.showSnack(ProfileEditActivity.this.getResources().getString(R.string.nameEmptyError), llEditProfileMain);
            retVal = false;
        }


        return retVal;

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ConstantUtils.REQUEST_CODE_GALLERY_IMAGES:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Uri selectedImage = data.getData();
                        /*String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = ProfileEditActivity.this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        if (cursor != null) {
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imgPath = cursor.getString(columnIndex);
                            cursor.close();
                        }*/
                        imgPath = FileUtils.getPath(ProfileEditActivity.this, selectedImage);
                        performCrop(imgPath);
                        //cursor.close();

                        /*
                         * *
                         * kalu     Uri selectedImageUri = data.getData();
                         imgPath = getPath(selectedImageUri);
                         performCrop(imgPath);**/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            case ConstantUtils.REQUEST_CODE_CAMERA_PIC:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        if (outFile.exists()) {
                            imgPath = outFile.getPath();
                            // Picasso.with(SignUpActivity.this).load(new File(imgPath)).into(circleView);
                            performCrop(imgPath);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            case ConstantUtils.REQUEST_CODE_CROP_PIC:
                if (resultCode == Activity.RESULT_OK) {
                    // get the returned data
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        Bitmap bitmap = extras.getParcelable("data");
                        Bitmap[] bitArr = {bitmap};
                        new SaveCroppedTask().execute(bitArr);
                    } else {
                        Picasso.with(ProfileEditActivity.this).load(new File(imgPath)).placeholder(R.drawable.icn_profile).into(profileEditImage);
                    }
                }
                break;

            default:
                break;
        }

    }


    @OnClick({R.id.profileEditImage, R.id.saveTxt, R.id.changePwdTxt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profileEditImage:
                new PickImageDialog(ProfileEditActivity.this, new ImageDialogClickListener() {
                    @Override
                    public void imageGalleryClick() {
                        openGalleryPhotos();
                    }

                    @Override
                    public void imageCameraClick() {
                        openCamera();
                    }
                }).show();
                break;
            case R.id.saveTxt:
                ApplicationUtils.getInstance().hideSoftKeyBoard(ProfileEditActivity.this);
                if (TextUtils.isEmpty(imgPath)) {
                    updateInfo();
                } else {
                    updateWithFile();
                }
                break;
            case R.id.changePwdTxt:
                switchActivity(ProfileEditActivity.this, ChangePasswordActivity.class);
                break;
        }
    }

    private void openCamera() {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            ApplicationUtils.showSnack(ProfileEditActivity.this.getResources().getString(R.string.no_sd_card), llEditProfileMain);
        } else {
            try {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri outUri = Uri.fromFile(outFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outUri);
                ProfileEditActivity.this.startActivityForResult(cameraIntent, ConstantUtils.REQUEST_CODE_CAMERA_PIC);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void openGalleryPhotos() {
        //   Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        ProfileEditActivity.this.startActivityForResult(photoPickerIntent, ConstantUtils.REQUEST_CODE_GALLERY_IMAGES);
    }

    //permission work
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ConstantUtils.MY_PERMISSION_CAMERA:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //openCamera();

                }// else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                //}
                break;

            case ConstantUtils.PERMISSION_READ_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //openCamera();

                }// else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                //}
                break;
            case ConstantUtils.PERMISSION_WRITE_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //openCamera();

                }// else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                //}
                break;
        }
    }

    public boolean checkLocationPermission(String permission) {
        int res = ProfileEditActivity.this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private void performCrop(String mCurrentPhotoPath) {
        try {
            // Initialize intent
            Intent intent = new Intent("com.android.camera.action.CROP");
// set data type to be sent
            intent.setType("image/*");
// get croppers available in the phone
            List<ResolveInfo> list = ProfileEditActivity.this.getPackageManager().queryIntentActivities(intent, 0);
            int size = list.size();
// handle the case if there's no cropper in the phone
            if (size == 0) {
                ApplicationUtils.showSnack("Can not find image crop app", llEditProfileMain);
                return;
            } else {
// now this is the case if cropper exists
// initialize the Uri for the captures or gallery image
                Uri imageUri = Uri.fromFile(new File(mCurrentPhotoPath));
                // Send the Uri path to the cropper intent
                intent.setData(imageUri);
                intent.putExtra("outputX", 200);
                intent.putExtra("outputY", 200);
                intent.putExtra("aspectX", 4);
                intent.putExtra("aspectY", 4);
                intent.putExtra("scale", true);
                // Here's my attempt to ask the intent to save output data as file
                //  File f = null;
                // Here I initialize empty file
                // This returns the file created
                // f = outFile;
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outFile));
            }
            // --------------------------------------------------------------------
            // -----------> When changing this to false it worked <----------------
            // --------------------------------------------------------------------
            intent.putExtra("return-data", true);
            // --------------------------------------------------------------------
            // --------------------------------------------------------------------
            // If there's only 1 Cropper in the phone (e.g. Gallery )
            if (size > 0) {
                // get the cropper intent found
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);
                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                startActivityForResult(i, ConstantUtils.REQUEST_CODE_CROP_PIC);
            }
        } catch (ActivityNotFoundException e) {
            ApplicationUtils.showSnack("Device doesn't support the crop action", llEditProfileMain);
        }
    }

    //below task saves bitmap as file on sd-card
    private class SaveCroppedTask extends AsyncTask<Bitmap, Void, String> {

        @Override
        protected String doInBackground(Bitmap... params) {
            String path;
            if (null != params[0]) {
                path = FileUtils.saveImage(ProfileEditActivity.this,params[0]);
            } else {
                path = imgPath;
            }
            return path;
        }

        @Override
        protected void onPostExecute(String path) {
            super.onPostExecute(path);
            System.out.println("savedBitmap>> " + path);
            imgPath = path;
            Picasso.with(ProfileEditActivity.this).load(new File(imgPath)).placeholder(R.drawable.icn_profile).into(profileEditImage);
        }



    }


    private void updateInfo() {
        String[] arrCheck = data();
        if (checkInputAvailable(arrCheck)) {
            Map<String, String> params = new HashMap<>();
            params.put("logintoken", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_USER_ID));
            params.put("first_name", getEditedText(firstNameEditEdt));
            params.put("last_name", getEditedText(lastNameEditEdt));
            params.put("mobile_no", getEditedText(mobileNumberEditEdt));
            new WebTask(ProfileEditActivity.this, WebUrls.BASE_API_URL + "user/update", params, new WebCompleteTask() {
                @Override
                public void onTimeOutError() {
                    ApplicationUtils.showSnack(ProfileEditActivity.this.getResources().getString(R.string.error_internet2), profileEditImage);
                }

                @Override
                public void onComplete(String response, int taskcode) {
                    try {
                        //{"user":{"id":"11","name":"kaluji khan","first_name":"kaluji","last_name":"khan","email":"khanrocks88@gmail.com","phone":"9829671952","image":"https:\/\/www.gtsinfosoft.com\/demo\/mudbugs\/uploads\/user\/11_58d91f5f9e7c2.jpg","thumb_image":"https:\/\/www.gtsinfosoft.com\/demo\/mudbugs\/uploads\/user\/thumb\/11_58d91f5f9e7c2.jpg"},"message":"User detail updated successfully!","status":1,"active":"Y"}
                        JSONObject jo = new JSONObject(response);
                        String status = ApplicationUtils.getkeyValue_Str(jo, "status");
                        if (status.equals("1")) {
                            JSONObject joInfo = jo.getJSONObject("user");
                            ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_FIRST_NAME, ApplicationUtils.getkeyValue_Str(joInfo, "first_name"));
                            ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_LAST_NAME, ApplicationUtils.getkeyValue_Str(joInfo, "last_name"));
                            ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHONE_NUMBER, ApplicationUtils.getkeyValue_Str(joInfo, "phone"));
                            ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHOTO_URL, ApplicationUtils.getkeyValue_Str(joInfo, "image"));

                            //  ApplicationUtils.showSnack(ProfileEditActivity.this.getResources().getString(R.string.profile_updated), profileEditImage);
                            //call method in main activity to refresh drawer name and image
                            Intent in = new Intent();
                            in.putExtra("image", ApplicationUtils.getkeyValue_Str(joInfo, "image"));
                            in.putExtra("name", ApplicationUtils.getkeyValue_Str(joInfo, "first_name"));
                            in.putExtra("lName", ApplicationUtils.getkeyValue_Str(joInfo, "last_name"));
                            in.putExtra("phone", ApplicationUtils.getkeyValue_Str(joInfo, "phone"));
                            setResult(ConstantUtils.RESULT_UPDATE_PROFILE, in);
                            finish();
                        } else {
                            if (jo.has("message"))
                                ApplicationUtils.showSnack(ApplicationUtils.getkeyValue_Str(jo, "message"), profileEditImage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, ConstantUtils.TASK_UPDATE);
        }
    }


    private void updateWithFile() {
        String[] arrCheck = data();
        if (checkInputAvailable(arrCheck)) {


            Map<String, String> params = new HashMap<>();
            params.put("logintoken", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_USER_ID));
            params.put("first_name", getEditedText(firstNameEditEdt));
            params.put("last_name", getEditedText(lastNameEditEdt));
            params.put("mobile_no", getEditedText(mobileNumberEditEdt));
            ApplicationUtils.spinnerStart(ProfileEditActivity.this);
            MultipartRequest multipartRequest = new MultipartRequest(WebUrls.BASE_API_URL + "user/update",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            ApplicationUtils.spinnerStop();
                            try {
                                JSONObject jo = new JSONObject(response);
                                String status = ApplicationUtils.getkeyValue_Str(jo, "status");
                                if (status.equals("1")) {
                                    JSONObject joInfo = jo.getJSONObject("user");
                                    ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_FIRST_NAME, ApplicationUtils.getkeyValue_Str(joInfo, "first_name"));
                                    ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_LAST_NAME, ApplicationUtils.getkeyValue_Str(joInfo, "last_name"));
                                    ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHONE_NUMBER, ApplicationUtils.getkeyValue_Str(joInfo, "phone"));
                                    ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHOTO_URL, ApplicationUtils.getkeyValue_Str(joInfo, "image"));
                                    //  ApplicationUtils.showSnack(ProfileEditActivity.this.getResources().getString(R.string.profile_updated), profileEditImage);
                                    //call method in main activity to refresh drawer name and image

                                    Intent in = new Intent();
                                    in.putExtra("image", ApplicationUtils.getkeyValue_Str(joInfo, "image"));
                                    in.putExtra("name", ApplicationUtils.getkeyValue_Str(joInfo, "first_name"));
                                    in.putExtra("lName", ApplicationUtils.getkeyValue_Str(joInfo, "last_name"));
                                    in.putExtra("phone", ApplicationUtils.getkeyValue_Str(joInfo, "phone"));
                                    setResult(ConstantUtils.RESULT_UPDATE_PROFILE, in);
                                    finish();
                                } else {
                                    if (jo.has("message"))
                                        ApplicationUtils.showSnack(ApplicationUtils.getkeyValue_Str(jo, "message"), profileEditImage);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ApplicationUtils.spinnerStop();
                    Log.d("error", error.toString());
                }
            }, new File(imgPath), "image", params);
            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    60000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            ApplicationUtils.getInstance().addToRequestQueue(multipartRequest);

        }
    }


}
