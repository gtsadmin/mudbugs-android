package com.mudbugshockey.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.ForgotDialog;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.views.EditTextNunitoRegular;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginActivity extends AbsGenericActivity implements WebCompleteTask {

    @Bind(R.id.toolLayout)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.emailEdt)
    EditTextNunitoRegular emailEdt;
    @Bind(R.id.passwordEdt)
    EditTextNunitoRegular passwordEdt;
    @Bind(R.id.llLogin)
    LinearLayout llLogin;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle("");
            toolbarTitle.setText(LoginActivity.this.getResources().getString(R.string.sign_in));
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.back_arrow);
        }
        //for testing

        String token = ApplicationUtils.getStringPrefs(ConstantUtils.KEY_DEVICE_TOKEN);
        System.out.println(">>>>>>>>>>>> "+token);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick({R.id.signinTxt, R.id.forgotTxt})
    public void onClick(View view) {
        ApplicationUtils.getInstance().produceAnimation(view);
        ApplicationUtils.getInstance().hideSoftKeyBoard(LoginActivity.this);
        switch (view.getId()) {
            case R.id.signinTxt:
                String[] arrCheck = data();
                if (checkInputAvailable(arrCheck)) {
                    Map<String, String> params = new HashMap<>();
                    params.put("email", getEditedText(emailEdt));
                    params.put("password", getEditedText(passwordEdt));
                    params.put("device_token", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_DEVICE_TOKEN));
                    params.put("device_type", "A");
                    new WebTask(LoginActivity.this, WebUrls.BASE_API_URL + "auth/login", params, LoginActivity.this,
                            ConstantUtils.TASK_DEFAULT);
                }

                break;
            case R.id.forgotTxt:
                new ForgotDialog(LoginActivity.this).show();
                break;
        }
    }

    private String[] data() {
        String[] arr = new String[2];
        arr[0] = getEditedText(emailEdt);
        arr[1] = getEditedText(passwordEdt);
        return arr;
    }

    private String getEditedText(EditText edt) {
        String s = edt.getText().toString();
        if (TextUtils.isEmpty(s)) {
            s = "";
        }
        return s;
    }

    private boolean checkInputAvailable(String[] ar) {
        boolean retVal = true;
        String email = ar[0];
        String password = ar[1];
        if (TextUtils.isEmpty(password) || password.length() < 5 || password.contains(" ")) {
            ApplicationUtils.showSnack(LoginActivity.this.getResources().getString(R.string.passwordError), llLogin);
            retVal = false;
        }
        if (!ApplicationUtils.isValidEmail(email)) {
            ApplicationUtils.showSnack(LoginActivity.this.getResources().getString(R.string.emailError), llLogin);
            retVal = false;
        }

        return retVal;

    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.e(ApplicationUtils.TAG, response);
        try {
            //{"message":"Email has been sent to registered email address. Please click on the URL in your
            // email to verify. Not received email? RESEND.","status":0,"active":"N"}
            JSONObject jo = new JSONObject(response);
            String status = ApplicationUtils.getkeyValue_Str(jo, "status");
            if (status.equals("1")) {
                //{"user":{"id":"6","name":"kalu khan","first_name":"kalu","last_name":"khan",
                // "email":"nano.kalu2013@gmail.com","phone":"9829671955",
                // "image":"https:\/\/www.gtsinfosoft.com\/demo\/mudbugs\/uploads\/user\/6_58d90f0d61f29.jpeg",
                // "thumb_image":"https:\/\/www.gtsinfosoft.com\/demo\/mudbugs\/uploads\/user\/thumb\/6_58d90f0d61f29.jpeg"},
                // "logintoken":"62572d4da4e08eca722946ca1729a3d5","message":"Logged In Successfully","status":1,"active":"Y"}
                JSONObject joInfo = jo.getJSONObject("user");
                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_USER_ID, ApplicationUtils.getkeyValue_Str(jo, "logintoken"));
                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_FIRST_NAME, ApplicationUtils.getkeyValue_Str(joInfo, "first_name"));
                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_LAST_NAME, ApplicationUtils.getkeyValue_Str(joInfo, "last_name"));
                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHONE_NUMBER, ApplicationUtils.getkeyValue_Str(joInfo, "phone"));
                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_PHOTO_URL, ApplicationUtils.getkeyValue_Str(joInfo, "image"));
                ApplicationUtils.saveStringPrefs(ConstantUtils.KEY_EMAIL, ApplicationUtils.getkeyValue_Str(joInfo, "email"));
                setResult(RESULT_OK);
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            } else {
                //{"message":"Incorrect Login","status":0,"active":"N"}
                if (jo.has("message"))
                    ApplicationUtils.simplePop(LoginActivity.this, ApplicationUtils.getkeyValue_Str(jo, "message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTimeOutError() {
        ApplicationUtils.showSnack(LoginActivity.this.getResources().getString(R.string.error_internet2), llLogin);
    }

}
