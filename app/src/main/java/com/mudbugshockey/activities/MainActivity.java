package com.mudbugshockey.activities;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.adapter.DrawerAdapter;
import com.mudbugshockey.adapter.NotificationRclAdapter;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.fragments.GeorgeFragment;
import com.mudbugshockey.fragments.MerchandiseFragment;
import com.mudbugshockey.fragments.NotificationsFragment;
import com.mudbugshockey.fragments.SeasonTicketFragment;
import com.mudbugshockey.fragments.SettingsFragment;
import com.mudbugshockey.fragments.SponsorshipRequestFragment;
import com.mudbugshockey.fragments.TabsFragment;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.mudbugshockey.wrapper.NotificationMainWrapper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AbsGenericActivity {

    @Bind(R.id.toolLayout)
    Toolbar toolbar;

    @Bind(R.id.frameContainer)
    FrameLayout frameContainer;

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @Bind(R.id.RecyclerView)
    RecyclerView mRecyclerView;

    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;
    private String titles[];
    public TextView mTitleTx;
    public TabsFragment tabsFragment;
    public static final int REQUEST_CODE_LOGOUT = 24;

    int icons[] = {R.drawable.menu_home, R.drawable.tab_buy, R.drawable.menu_marchandise,
            R.drawable.menu_sponsor, R.drawable.tab_roster, R.drawable.menu_setting, R.drawable.menu_notification};
    //private RecyclerView.Adapter mAdapter;
    private DrawerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        titles = MainActivity.this.getResources().getStringArray(R.array.nav_drawer_items);
        //android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.tool_bar);
        mTitleTx = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setDisplayShowTitleEnabled(false);
        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size
        mAdapter = new DrawerAdapter(MainActivity.this, titles, icons, ApplicationUtils.getStringPrefs(ConstantUtils.KEY_PHOTO_URL), ApplicationUtils.getStringPrefs(ConstantUtils.KEY_FIRST_NAME), new RecycleClickListener() {
            @Override
            public void onRecycleClick(int position) {
                displayView(position);
            }
        });       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture
        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);// Creating a layout Manager
        mRecyclerView.setLayoutManager(mLayoutManager);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
//                mTitleTx.setText("");

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
//                mTitleTx.setText(mTitle);

            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        tabsFragment = new TabsFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frameContainer, tabsFragment).commit();
        mTitleTx.setText(MainActivity.this.getResources().getString(R.string.shreveport_mudbugs));

        if (!ApplicationUtils.getBooleanPrefs(ConstantUtils.KEY_IS_TOKEN_SAVED)){
            insertGCMToken();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        ApplicationUtils.getInstance().setIsForeground(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_upload).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        // update the main content by replacing fragments
        if (position != 0) {
            if (position == 1) {
                setTitle(MainActivity.this.getResources().getString(R.string.shreveport_mudbugs));
            } else {
                mTitle = titles[position - 1];
                setTitle(titles[position - 1]);
            }

        }
        mDrawerLayout.closeDrawer(mRecyclerView);
        switch (position) {
            case 0://profile
                Intent in = new Intent(MainActivity.this, ProfileActivity.class);
                startActivityForResult(in, REQUEST_CODE_LOGOUT);
                break;
            case 1://home
                fragment = new TabsFragment();
                break;
            case 2://Season Tickets
                fragment = new SeasonTicketFragment();
                break;
            case 3://Merchandise
                fragment = new MerchandiseFragment();
                break;
            case 4://sponsorship
                fragment = new SponsorshipRequestFragment();
                break;
            case 5://george's pond
                fragment = new GeorgeFragment();
                break;
            case 6://settings
                fragment = new SettingsFragment();
                break;
            case 7://notification
                fragment = new NotificationsFragment();
                break;
            default:
                break;

        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frameContainer, fragment).commit();

        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        mTitleTx.setText(mTitle);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content
        // view
        // boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        // menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        // boolean drawerOpen = mDrawerLayout.isDrawerOpen(drawerFragment.getView());
        //  menu.findItem(R.id.action_info).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Picasso.with(this).cancelTag(this);
        ApplicationUtils.getInstance().setIsForeground(false);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment frag = fragmentManager.findFragmentById(R.id.frameContainer);
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawers();
        } else if (frag instanceof TabsFragment) {
            ((TabsFragment) frag).getBackTicketsWeb();
        } else {
            backPressAlert();
        }
    }

    boolean doubleBackToExitPressedOnce = false;

    public void backPressAlert() {
        if (doubleBackToExitPressedOnce) {
            finish();
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.tap_again_to_exit, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 3000);
    }

    private void notifyDrawerAdapter(String imageUrl, String displayName) {
        mAdapter.notifyMe(imageUrl, displayName);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment frag = fragmentManager.findFragmentById(R.id.frameContainer);
        if (frag instanceof SettingsFragment) {
            frag.onActivityResult(requestCode, resultCode, data);
        }
        switch (requestCode) {
            case REQUEST_CODE_LOGOUT:
                if (resultCode == RESULT_OK) {
                    finish();
                } else if (resultCode == ConstantUtils.RESULT_UPDATE_PROFILE) {
                    if (data != null) {
                        notifyDrawerAdapter(data.getStringExtra("image"), data.getStringExtra("name"));
                    }
                }
                break;
        }
    }

    private void insertGCMToken() {
        Map<String, String> params = new HashMap<>();
        params.put("logintoken", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_USER_ID));
        params.put("device_token", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_DEVICE_TOKEN));
        params.put("device_type", "A");
        new WebTask(MainActivity.this, WebUrls.BASE_API_URL + "user/device", params, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                String status = "";
                try {
                    JSONObject jo = new JSONObject(response);
                    status = ApplicationUtils.getkeyValue_Str(jo, "status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (status.equals("1")) {
                    Log.e("insertGCMToken", response);
                    ApplicationUtils.saveBooleanPrefs(ConstantUtils.KEY_IS_TOKEN_SAVED,true);
                }

            }

            @Override
            public void onTimeOutError() {}
        },false, 1);
    }
}
