package com.mudbugshockey.activities;

public interface ConfirmDialogListener {
    void onConfirmClicked(int confirmWhat);
    void onCancelClick(int cancelWhat);
}
