package com.mudbugshockey.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public abstract class AbsGenericActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
    }


    public void onBackPressed()
    {
        super.onBackPressed();
        finish();

    }

    protected void switchActivity(Activity paramActivity, Class<?> paramClass)
    {
        try
        {
            paramActivity.startActivity(new Intent(paramActivity, paramClass));
            return;
        }
        catch (Exception localException)
        {
            localException.printStackTrace();
        }
    }
    protected void switchActivity(Activity paramActivity, Class<?> paramClass,Bundle extras)
    {
        try
        {
            Intent intent = new Intent(paramActivity, paramClass);
            intent.putExtras(extras);
            paramActivity.startActivity(intent);

            return;
        }
        catch (Exception localException)
        {
            localException.printStackTrace();
        }
    }
}
