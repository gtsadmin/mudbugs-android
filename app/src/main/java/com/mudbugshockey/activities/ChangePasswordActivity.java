package com.mudbugshockey.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends AbsGenericActivity {
    @Bind(R.id.toolLayout)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.llChangePass)
    LinearLayout llChangePass;

    @Bind(R.id.passwordEdt)
    com.mudbugshockey.views.EditTextNunitoRegular passwordEdt;
    @Bind(R.id.newPasswordEdt)
    com.mudbugshockey.views.EditTextNunitoRegular newPasswordEdt;

    private boolean isEyeOpen = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle("");
            toolbarTitle.setText(getResources().getString(R.string.changePwd));
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.back_arrow);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_upload).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private String[] data() {
        String[] arr = new String[2];
        arr[0] = getEditedText(passwordEdt);
        arr[1] = getEditedText(newPasswordEdt);

        return arr;

    }

    private String getEditedText(EditText edt) {
        String s = edt.getText().toString();
        if (TextUtils.isEmpty(s)) {
            s = "";
        }
        return s;
    }

    private boolean checkInputAvailable(String[] ar) {
        boolean retVal = true;
        String pass = ar[0];
        String newPass = ar[1];
        if (TextUtils.isEmpty(newPass) || newPass.length() < 5 || newPass.contains(" ")) {
            ApplicationUtils.showSnack(ChangePasswordActivity.this.getResources().getString(R.string.passwordError), llChangePass);
            retVal = false;
        }
        if (TextUtils.isEmpty(pass) || pass.length() < 5 || pass.contains(" ")) {
            ApplicationUtils.showSnack(ChangePasswordActivity.this.getResources().getString(R.string.passwordError), llChangePass);
            retVal = false;
        }


        return retVal;

    }

    @OnClick({R.id.changePassTxt, R.id.imgPwdVisible})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.changePassTxt:
                ApplicationUtils.getInstance().hideSoftKeyBoard(ChangePasswordActivity.this);
                updateInfo();
                break;
            case R.id.imgPwdVisible:
                isEyeOpen = !isEyeOpen;
                ImageView imgEye = (ImageView) view;
                if (isEyeOpen) {
                    imgEye.setImageResource(R.drawable.eye);
                    newPasswordEdt.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    imgEye.setImageResource(R.drawable.closedeye);
                    newPasswordEdt.setInputType(129);
                }
                break;
        }
    }

    private void updateInfo() {
        String[] arrCheck = data();
        if (checkInputAvailable(arrCheck)) {
            Map<String, String> params = new HashMap<>();
            params.put("logintoken", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_USER_ID));
            params.put("old_password", getEditedText(passwordEdt));
            params.put("new_password", getEditedText(newPasswordEdt));
            new WebTask(ChangePasswordActivity.this, WebUrls.BASE_API_URL + "user/change_password", params, new WebCompleteTask() {
                @Override
                public void onTimeOutError() {
                    ApplicationUtils.showSnack(ChangePasswordActivity.this.getResources().getString(R.string.error_internet2), llChangePass);
                }

                @Override
                public void onComplete(String response, int taskcode) {
                    try {
                        JSONObject jo = new JSONObject(response);
                        String status = ApplicationUtils.getkeyValue_Str(jo, "status");
                        if (status.equals("1")) {
                            ApplicationUtils.showMessage(ChangePasswordActivity.this, ChangePasswordActivity.this.getResources().getString(R.string.password_updated));
                            finish();
                        } else {
                            if (jo.has("message"))
                                ApplicationUtils.showSnack(ApplicationUtils.getkeyValue_Str(jo, "message"), llChangePass);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, ConstantUtils.TASK_UPDATE);
        }
    }


}
