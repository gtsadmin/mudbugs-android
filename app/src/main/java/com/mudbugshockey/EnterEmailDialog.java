package com.mudbugshockey;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mudbugshockey.application.ApplicationUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class EnterEmailDialog extends Dialog {
    EnterEmailListener listener;
    Context ctx;

    @Bind(R.id.dlgEnterEmail)
    LinearLayout dlgEnterEmail;
    @Bind(R.id.emailDialogEdt)
    com.mudbugshockey.views.EditTextNunitoRegular emailDialogEdt;
    String[] arrVal;

    public EnterEmailDialog(Context context, String[] arr, EnterEmailListener listener_) {
        super(context, R.style.Theme_Custom);
        ctx = context;
        this.arrVal = arr;
        this.listener = listener_;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_enter_email);
        ButterKnife.bind(this);

    }

    /**
     * check the email address format and other fields
     */

    private String[] data() {
        String[] arr = new String[1];
        arr[0] = getEditedText(emailDialogEdt);
        return arr;
    }

    private String getEditedText(EditText edt) {
        String s = edt.getText().toString();
        if (TextUtils.isEmpty(s)) {
            s = "";
        }
        return s;
    }

    private boolean checkInputAvailable(String[] ar) {
        boolean retVal = true;
        String email = ar[0];

        if (!ApplicationUtils.isEmailValid(email)) {
            ApplicationUtils.showSnack(ctx.getResources().getString(R.string.emailError),dlgEnterEmail);
            retVal = false;
        }

        return retVal;

    }


    @OnClick(R.id.btnEnter)
    public void onClick() {

        String[] arrCheck = data();
        if (checkInputAvailable(arrCheck)) {
            dismiss();
            hideKeyboard();
            arrVal[3] = emailDialogEdt.getText().toString();
            listener.onEmailEnter(arrVal);
        }
    }
    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(emailDialogEdt.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
