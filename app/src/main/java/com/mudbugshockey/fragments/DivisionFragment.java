package com.mudbugshockey.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mudbugshockey.R;
import com.mudbugshockey.adapter.PreppingAdapter;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.mudbugshockey.wrapper.Row;
import com.mudbugshockey.wrapper.StandingsWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DivisionFragment extends Fragment {

    @Bind(R.id.listDivision)
    com.mudbugshockey.views.PinnedHeaderListView lvPinned;
    @Bind(R.id.swpLayoutDivision)
    android.support.v4.widget.SwipeRefreshLayout swpLayoutStanding;
    @Bind(R.id.llDivision)
    LinearLayout llDivision;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_division, container, false);
        ButterKnife.bind(this, rootView);
        View footer = getActivity().getLayoutInflater().inflate(R.layout.footer_standings, null);
        lvPinned.addFooterView(footer);
        if (ApplicationUtils.getInstance().getSections().size() > 0) {
            PreppingAdapter adapter = new PreppingAdapter(getActivity(), ApplicationUtils.getInstance().getSections());
            lvPinned.setAdapter(adapter);// Setting the adapter to RecyclerView
        }else{
            getStandings();
        }


        swpLayoutStanding.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getStandings();
            }
        });
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void getStandings() {
        String param = "feed=statviewfeed&view=teams&groupTeamsBy=division&context=overall&site_id=2&season="+ApplicationUtils.getInstance().getSeasonId()+"&special=false&key=bb92e4a0d0be791d&client_code=nahl&league_id=1&division=undefined&sort=points";
        Map<String, String> params = new HashMap<>();
        new WebTask(getActivity(), WebUrls.BASE_URL + param, params, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                cancelRefreshing();
                JSONObject jo;
                List<StandingsWrapper> sections = new ArrayList<>();
                try {
                    if (!TextUtils.isEmpty(response)) {
                        response = response.substring(1, response.length() - 1);
                    }
                    JSONArray joArray = new JSONArray(response);
                    for (int i = 0; i < joArray.length(); i++) {
                        jo = joArray.getJSONObject(i);
                        JSONArray sectionArray = jo.getJSONArray("sections");
                        for (int j = 0; j < sectionArray.length(); j++) {
                            JSONObject joSec = sectionArray.getJSONObject(j);

                            //data
                            StandingsWrapper sectionWp = new StandingsWrapper();
                            JSONArray dataArray = joSec.getJSONArray("data");
                            for (int k = 0; k < dataArray.length(); k++) {
                                JSONObject joData = dataArray.getJSONObject(k);
                                JSONObject joRow = joData.getJSONObject("row");
                                Row row = new Row();
                                row.setTeam_code(ApplicationUtils.getkeyValue_Str(joRow, "team_code"));
                                row.setWins(ApplicationUtils.getkeyValue_Str(joRow, "wins"));
                                row.setLosses(ApplicationUtils.getkeyValue_Str(joRow, "losses"));
                                row.setOt_losses(ApplicationUtils.getkeyValue_Str(joRow, "ot_losses"));
                                row.setOt_wins(ApplicationUtils.getkeyValue_Str(joRow, "ot_wins"));
                                row.setShootout_losses(ApplicationUtils.getkeyValue_Str(joRow, "shootout_losses"));
                                row.setRegulation_wins(ApplicationUtils.getkeyValue_Str(joRow, "regulation_wins"));
                                row.setRow(ApplicationUtils.getkeyValue_Str(joRow, "row"));
                                row.setPoints(ApplicationUtils.getkeyValue_Str(joRow, "points"));
                                row.setPenalty_minutes(ApplicationUtils.getkeyValue_Str(joRow, "penalty_minutes"));
                                row.setStreak(ApplicationUtils.getkeyValue_Str(joRow, "streak"));
                                row.setGoals_for(ApplicationUtils.getkeyValue_Str(joRow, "goals_for"));
                                row.setGoals_against(ApplicationUtils.getkeyValue_Str(joRow, "goals_against"));
                                row.setGoals_diff(ApplicationUtils.getkeyValue_Str(joRow, "goals_diff"));
                                row.setPercentage(ApplicationUtils.getkeyValue_Str(joRow, "percentage"));
                                row.setGames_played(ApplicationUtils.getkeyValue_Str(joRow, "games_played"));
                                row.setRank(ApplicationUtils.getkeyValue_Str(joRow, "rank"));
                                row.setPast_10(ApplicationUtils.getkeyValue_Str(joRow, "past_10"));
                                row.setName(ApplicationUtils.getkeyValue_Str(joRow, "name"));
                                sectionWp.data.add(row);
                            }
                            sections.add(sectionWp);
                            //headers
                            JSONObject joHeaders = joSec.getJSONObject("headers");
                            JSONObject joName = joHeaders.getJSONObject("name");
                            JSONObject joProperties = joName.getJSONObject("properties");
                            String title = ApplicationUtils.getkeyValue_Str(joProperties, "title");
                            sectionWp.setTitleName(title);
                        }


                    }

                    ApplicationUtils.getInstance().setSections(sections);
                    PreppingAdapter adapter = new PreppingAdapter(getActivity(), sections);
                    lvPinned.setAdapter(adapter);// Setting the adapter to RecyclerView
                    //-----------------------------------------------------------------------------------------
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTimeOutError() {
                try {
                    cancelRefreshing();
                    ApplicationUtils.showSnack(getActivity().getResources().getString(R.string.error_internet2),
                            llDivision);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1);
    }

    private void cancelRefreshing() {
        if (swpLayoutStanding != null)
            swpLayoutStanding.setRefreshing(false);
    }

}
