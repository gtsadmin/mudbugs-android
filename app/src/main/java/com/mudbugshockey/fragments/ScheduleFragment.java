package com.mudbugshockey.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.adapter.ScheduleRclAdapter;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.mudbugshockey.wrapper.ScheduleMainWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ScheduleFragment extends Fragment {

    @Bind(R.id.rclSchedule)
    RecyclerView rclSchedule;
    @Bind(R.id.swpLayoutSchedule)
    android.support.v4.widget.SwipeRefreshLayout swpLayoutSchedule;
    @Bind(R.id.llSchedule)
    LinearLayout llSchedule;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_schedule, container, false);
        ButterKnife.bind(this, v);
        // Creating a layout Manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        rclSchedule.setLayoutManager(mLayoutManager);

        getSchedule();
        swpLayoutSchedule.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSchedule();
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void getSchedule() {
        String url = "http://cluster.leaguestat.com/feed/index.php?feed=modulekit&key=bb92e4a0d0be791d&site_id=2&client_code=nahl&view=scorebar&team_id=24&numberofdaysahead=365&numberofdaysback=365";
        // String param = "feed=modulekit&view=schedule&key=bb92e4a0d0be791d&site_id=2&client_code=nahl&league_code=1&lang=en&fmt=json&team_id=24";
        Map<String, String> params = new HashMap<>();
        new WebTask(getActivity(), url, params, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                cancelRefreshing();
                try {
                    JSONObject skObj = null;
                    try {
                        JSONObject jo = new JSONObject(response);
                        skObj = jo.getJSONObject("SiteKit");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (skObj == null) return;
                    Gson gson = new Gson();
                    ScheduleMainWrapper wrapper = gson.fromJson(skObj.toString(), ScheduleMainWrapper.class);
                    ScheduleRclAdapter adapter = new ScheduleRclAdapter(getActivity(), llSchedule,
                            wrapper.Scorebar, new RecycleClickListener() {
                        @Override
                        public void onRecycleClick(int position) {
                            // startActivity(new Intent(getActivity(), GameResultActivity.class));
                        }
                    });
                    rclSchedule.setAdapter(adapter);// Setting the adapter to RecyclerView
                    //wrapper.final_.equals("1")
                    int ind = 0;
                    for (int i = 0; i < wrapper.Scorebar.size(); i++) {
                        if (!wrapper.Scorebar.get(i).GameStatusStringLong.contains("Final")) {
                            ind = i;
                            break;
                        }
                    }
                    if (ind > 3)
                        mLayoutManager.scrollToPosition((ind - 3));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTimeOutError() {
                try {
                    cancelRefreshing();
                    ApplicationUtils.showSnack(getActivity().getResources().getString(R.string.error_internet2),
                            llSchedule);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1);
    }

    private void cancelRefreshing() {
        if (swpLayoutSchedule != null)
            swpLayoutSchedule.setRefreshing(false);
    }

}
