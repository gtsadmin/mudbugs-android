package com.mudbugshockey.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.mudbugshockey.R;
import com.mudbugshockey.activities.MainActivity;
import com.mudbugshockey.webutility.WebUrls;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TicketsFragment extends Fragment {

    @Bind(R.id.webView_)
    WebView webView;
    WebSettings webSettings;
    @Bind(R.id.progressBar_)
    ProgressBar progressBar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View mRoot = inflater.inflate(R.layout.fragment_merchandise, container, false);
        ButterKnife.bind(this, mRoot);
        webSettings = webView.getSettings();
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webView.loadUrl(WebUrls.TICKETS_URL);
        webView.setWebViewClient(new WebViewClientClass());
        return mRoot;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void backWeb() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            ((MainActivity) getActivity()).backPressAlert();
        }
    }

    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            try {
                progressBar.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            try {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                //view.loadUrl("var style=document.createElement('style');style.innerHTML='#leaguenetwork,.header-section,#masthead,.scoreboardheader,footer{display:none;}';document.head.appendChild(style)");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
