package com.mudbugshockey.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.adapter.NotificationRclAdapter;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.mudbugshockey.wrapper.NotificationMainWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NotificationsFragment extends Fragment {

    @Bind(R.id.rclNotification)
    RecyclerView rclNotification;
    @Bind(R.id.swpLayoutNtf)
    SwipeRefreshLayout swpLayout;
    @Bind(R.id.llNotification)
    LinearLayout llNotification;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rosterView = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, rosterView);

        // Creating a layout Manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rclNotification.setLayoutManager(mLayoutManager);
        getNotifications();
        swpLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNotifications();
            }
        });
        return rosterView;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void getNotifications() {
        Map<String, String> params = new HashMap<>();
        params.put("logintoken", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_USER_ID));
        new WebTask(getActivity(), WebUrls.BASE_API_URL + "user/notifications", params, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                cancelRefreshing();
                String status = "";
                try {
                    JSONObject jo = new JSONObject(response);
                    status = ApplicationUtils.getkeyValue_Str(jo,"status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (status.equals("1")) {
                    Gson gson = new Gson();
                    NotificationMainWrapper wrapper = gson.fromJson(response, NotificationMainWrapper.class);
                    NotificationRclAdapter adapter = new NotificationRclAdapter(getActivity(),
                            wrapper.notifications, new RecycleClickListener() {
                        @Override
                        public void onRecycleClick(int position) {

                        }
                    });
                    rclNotification.setAdapter(adapter);// Setting the adapter to RecyclerView
                }

            }

            @Override
            public void onTimeOutError() {
                try {
                    cancelRefreshing();
                    ApplicationUtils.showSnack(getActivity().getResources().getString(R.string.error_internet2),
                            llNotification);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1);
    }

    private void cancelRefreshing() {
        if (swpLayout != null)
            swpLayout.setRefreshing(false);
    }
}
