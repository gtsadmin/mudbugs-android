package com.mudbugshockey.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.adapter.TicketFlyRclAdapter;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.mudbugshockey.wrapper.TicketMainWrapper;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BuyFragment extends Fragment {


    @Bind(R.id.rclSchedule_)
    RecyclerView rclSchedule;
    @Bind(R.id.swpLayoutSchedule_)
    android.support.v4.widget.SwipeRefreshLayout swpLayoutSchedule;
    @Bind(R.id.llBuy)
    LinearLayout llBuy;
    private RecyclerView.LayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View mRoot = inflater.inflate(R.layout.fragment_buy, container, false);
        ButterKnife.bind(this, mRoot);

        // Creating a layout Manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        rclSchedule.setLayoutManager(mLayoutManager);

        getData();
        swpLayoutSchedule.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
        return mRoot;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void getData() {
        String param = "upcoming.json?orgId=6847&fields=id,name,startDate,ticketPurchaseUrl,eventStatus,ticketPrice,image";
        Map<String, String> params = new HashMap<>();
        new WebTask(getActivity(), WebUrls.TICKET_FLY_URL + param, params, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                cancelRefreshing();
                try {
                    Gson gson = new Gson();
                    TicketMainWrapper wrapper = gson.fromJson(response, TicketMainWrapper.class);
                    TicketFlyRclAdapter adapter = new TicketFlyRclAdapter(getActivity(),
                            wrapper.events, new RecycleClickListener() {
                        @Override
                        public void onRecycleClick(int position) {
                            // startActivity(new Intent(getActivity(), GameResultActivity.class));
                        }
                    });

                    rclSchedule.setAdapter(adapter);// Setting the adapter to RecyclerView
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onTimeOutError() {
                try {
                    cancelRefreshing();
                    ApplicationUtils.showSnack(getActivity().getResources().getString(R.string.error_internet2),
                            llBuy);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1);
    }

    private void cancelRefreshing() {
        if (swpLayoutSchedule != null)
            swpLayoutSchedule.setRefreshing(false);
    }

}
