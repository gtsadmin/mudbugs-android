package com.mudbugshockey.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mudbugshockey.R;
import com.mudbugshockey.adapter.ViewPagerAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;


public class StandingsFragment extends Fragment {

    @Bind(R.id.tabsStandings)
    android.support.design.widget.TabLayout tabsStandings;
    @Bind(R.id.vpStandings)
    android.support.v4.view.ViewPager pager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_standings, container, false);
        ButterKnife.bind(this, v);
        //ScoringShots work starts
        Fragment[] fragArr = {new DivisionFragment(), new LeagueFragment()};
        String tabTitles[] = new String[]{"DIVISION", "LEAGUE"};
        pager.setAdapter(new ViewPagerAdapter(getFragmentManager(), fragArr, tabTitles));

        final TabLayout.Tab division = tabsStandings.newTab();
        final TabLayout.Tab league = tabsStandings.newTab();
        division.setText(getActivity().getResources().getString(R.string.division));
        league.setText(getActivity().getResources().getString(R.string.league));
        tabsStandings.addTab(division, 0);
        tabsStandings.addTab(league, 1);
        tabsStandings.setTabTextColors(ContextCompat.getColorStateList(getActivity(), R.color.white));
        tabsStandings.setSelectedTabIndicatorColor(ContextCompat.getColor(getActivity(), R.color.white));
        tabsStandings.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(pager));
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabsStandings));
        /*
        * *pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                       // tabsStandings.getChildAt(0).setBackgroundColor(Color.parseColor("#f1a026"));
                        break;
                    case 1:
                       // tabsStandings.getChildAt(1).setBackgroundColor(Color.parseColor("#f1a026"));
                        break;
                    case 2:
                        //tabsStandings.getChildAt(2).setBackgroundColor(Color.parseColor("#f1a026"));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
**/
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
