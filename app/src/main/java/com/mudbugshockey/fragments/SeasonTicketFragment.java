package com.mudbugshockey.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SeasonTicketFragment extends Fragment {

    @Bind(R.id.rlSeason)
    RelativeLayout rlSeason;
    @Bind(R.id.llWebView_)
    LinearLayout llWebView_;

    private WebView webView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View mRoot = inflater.inflate(R.layout.fragment_season_tickets, container, false);
        ButterKnife.bind(this, mRoot);
        webView = new WebView(getActivity());
        webView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
//		webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        //webView.setBackgroundColor(ContextCompat.getColor(TermsPolicyActivity.this, R.color.grey));
        getTextServer();
        return mRoot;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    //server action
    private void getTextServer() {
        Map<String, String> params = new HashMap<>();
        params.put("logintoken", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_USER_ID));
        new WebTask(getActivity(), WebUrls.BASE_API_URL + "user/pages", params, new WebCompleteTask() {
            @Override
            public void onTimeOutError() {
                ApplicationUtils.showSnack(getActivity().getResources().getString(R.string.error_internet2), rlSeason);
            }

            @Override
            public void onComplete(String response, int taskcode) {
                try {

                    JSONObject jo = new JSONObject(response);
                    String status = ApplicationUtils.getkeyValue_Str(jo, "status");
                    if (status.equals("1")) {
                        JSONArray ja = jo.getJSONArray("pages");
                        String text = "";
                        for (int i = 0; i < ja.length(); i++) {
                            JSONObject jObj = ja.getJSONObject(i);
                            String title = ApplicationUtils.getkeyValue_Str(jObj, "title");
                            if (title.equals("Season Tickets"))
                                text = ApplicationUtils.getkeyValue_Str(jObj, "description");

                        }
                        if (webView != null)
                            llWebView_.removeView(webView);
                        llWebView_.addView(webView);
                        String mimeType = "text/html";
                        String encoding = "UTF-8";
                        if (!TextUtils.isEmpty(text))
                            webView.loadDataWithBaseURL("", getHtmlData(text), mimeType, encoding, "");
                    } else {
                        if (jo.has("message"))
                            ApplicationUtils.showSnack(ApplicationUtils.getkeyValue_Str(jo, "message"), rlSeason);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, ConstantUtils.TASK_DEFAULT);
    }

    private String getHtmlData(String data) {// SonySketchEF
        String head = "<style type=\"text/css\">@font-face {font-family: Nunito;src: url(\"file:///android_asset/fonts/Nunito-Regular.ttf\")}body,table{font-family:'Nunito';font-size:27pt;background:#ffffff;color:black;}table{font-size:25px}strong{display:block;font-size:1.2em;text-transform:uppercase;text-align:left}</style>";
        String htmlData = "<html>" + head + "<body>" + data + "</body></html>";
        return htmlData;
    }

}
