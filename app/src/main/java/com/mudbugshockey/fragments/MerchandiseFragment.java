package com.mudbugshockey.fragments;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.mudbugshockey.R;
import com.mudbugshockey.webutility.WebUrls;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MerchandiseFragment extends Fragment {

    @Bind(R.id.webView_)
    WebView webView;
    WebSettings webSettings;
    //String ua = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0";
    @Bind(R.id.progressBar_)
    ProgressBar progressBar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View mRoot = inflater.inflate(R.layout.fragment_merchandise, container, false);
        ButterKnife.bind(this, mRoot);
        webSettings = webView.getSettings();
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        //webSettings.setLoadWithOverviewMode(true);
        // webSettings.setUseWideViewPort(true);
        //  webSettings.setUserAgentString(ua);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // chromium, enable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.loadUrl(WebUrls.MERCHANDISE_URL);
        webView.setWebViewClient(new WebViewClientClass());
        return mRoot;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            try {
                // ApplicationUtils.spinnerStart(getActivity());
                progressBar.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            try {
                if (progressBar != null) {
                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            if (progressBar != null)
                                progressBar.setVisibility(View.GONE);

                        }
                    }, 9000);
                }
                // view.loadUrl("var style=document.createElement('style');style.innerHTML='#leaguenetwork,.header-section,#masthead,.scoreboardheader,footer{display:none;}';document.head.appendChild(style)");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
