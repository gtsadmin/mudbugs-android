package com.mudbugshockey.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.adapter.HomeRclAdapter;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.views.TextViewNunitoRegular;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.mudbugshockey.wrapper.NewsMainWrapper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;


public class HomeFragment extends Fragment {


    @Bind(R.id.leftLogoHomeImg)
    ImageView leftLogoImg;
    @Bind(R.id.daysTxt)
    TextViewNunitoRegular daysTxt;
    @Bind(R.id.hourTxt)
    TextViewNunitoRegular hourTxt;
    @Bind(R.id.minTxt)
    TextViewNunitoRegular minTxt;
    @Bind(R.id.secTxt)
    TextViewNunitoRegular secTxt;
    @Bind(R.id.rightLogoHomeImg)
    ImageView rightLogoImg;
    @Bind(R.id.rclHome)
    RecyclerView rclHome;
    @Bind(R.id.swpLayoutHome)
    SwipeRefreshLayout swpLayoutHome;
    @Bind(R.id.llHome_)
    LinearLayout llHome;
    @Bind(R.id.leftFrame)
    FrameLayout leftFrame;
    @Bind(R.id.rightFrame)
    FrameLayout rightFrame;

    private CountDownTimer timer;

    //HomeHeaderWrapper homeHeaderWrapper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, v);
        // Creating a layout Manager
        // Creating a layout Manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rclHome.setLayoutManager(mLayoutManager);
        swpLayoutHome.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cancelRefreshing();
            }
        });


        getTime();
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void getNews() {
        if (getActivity() == null) return;
        // String url = "https://graph.facebook.com/v2.8/1225628440868433/feed?fields=id,story,message,picture,full_picture,source,type&access_token=EAASG23Fwh4gBANME7QI1BmgvfoO4NfDv5w4VsDirh1e676NqAMINMjT8OBNh3Hiy5gujDG5Rid9IR4qWZB9CTZBZAhQ7664nBjS62x2zuNl8xUBB5Qb2kH5Dg2WZBvbdAZCDS2XcR1GnQ5dqgWf4JfQLiGO0nfh8JAmufZAmUZCc516Fmsk8K3t";
        String url = "https://graph.facebook.com/v2.8/294601745183/feed?fields=id,story,message,picture,full_picture,name,source,type&access_token=EAAJgIc4Eh6ABAIFh9q8QFuqfoJrMtySuMOOKR1fdeuDRG0q2rYUO0vVuCQ35CAdpCZBaE5ZB6Xufd9jG0ZCGb4sQXpRAY1jNZCITG6WbGpM5NDFQSbsIh1n7FZAKnxu4a6cY6njUbPjxZCYfyxLzHFZBUGc3ZAjSS8FQHNQV7KOKGwZDZD";
        new WebTask(getActivity(), url, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                cancelRefreshing();
                try {
                    HomeRclAdapter adapter;
                    JSONObject jo = null;
                    try {
                        jo = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Gson gson = new Gson();//homeHeaderWrapper,
                    NewsMainWrapper wrapper = gson.fromJson(jo.toString(), NewsMainWrapper.class);
                    adapter = new HomeRclAdapter(getActivity(),
                            wrapper.data, new RecycleClickListener() {
                        @Override
                        public void onRecycleClick(int position) {

                        }
                    });
                    rclHome.setAdapter(adapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onTimeOutError() {
                try {
                    cancelRefreshing();
                    ApplicationUtils.showSnack(getActivity().getResources().getString(R.string.error_internet2),
                            llHome);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getTime() {

        TimeZone tz = TimeZone.getDefault();
        String current_Time_Zone = ApplicationUtils.getGmtOffsetString(tz.getRawOffset());
        Map<String, String> params = new HashMap<>();
        params.put("logintoken", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_USER_ID));
        params.put("local_time", current_Time_Zone);
        new WebTask(getActivity(), WebUrls.BASE_API_URL + "user/upcoming_match", params, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                Log.e(ApplicationUtils.TAG, response);
                try {

                    JSONObject jo = new JSONObject(response);
                    String status = ApplicationUtils.getkeyValue_Str(jo, "status");
                    if (status.equals("1")) {
                        String imageSeasonId = ApplicationUtils.getkeyValue_Str(jo, "image_season_id");
                        ApplicationUtils.getInstance().setImage_seasonId(imageSeasonId);
                        String seasonId = ApplicationUtils.getkeyValue_Str(jo, "season_id");
                        ApplicationUtils.getInstance().setSeasonId(seasonId);
                        JSONObject joData = jo.getJSONObject("data");
                        String season_id = ApplicationUtils.getkeyValue_Str(joData, "season_id");
                        String home_team = ApplicationUtils.getkeyValue_Str(joData, "home_team");
                        String visiting_team = ApplicationUtils.getkeyValue_Str(joData, "visiting_team");
                        String date_time = ApplicationUtils.getkeyValue_Str(joData, "date_time");
                        String venueName = ApplicationUtils.getkeyValue_Str(joData, "venue_name");
                        long millis = ApplicationUtils.getInstance().getMillisTime(date_time, "yyyy-MM-dd HH:mm:ss");
                        long current = System.currentTimeMillis();
                        long diff = millis - current;

                        //  if (!TextUtils.isEmpty(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_PHOTO_URL)))
                        try {
                            if (leftLogoImg != null && home_team.equals("24")) {//SHV mudbugs - home team
                                leftFrame.setVisibility(View.VISIBLE);
                                rightFrame.setVisibility(View.VISIBLE);
                                Picasso.with(getActivity()).load(WebUrls.IMAGE_BASE_URL + home_team + "_" + imageSeasonId + ".jpg").placeholder(R.drawable.logo_signup).into(leftLogoImg);
                                Picasso.with(getActivity()).load(WebUrls.IMAGE_BASE_URL + visiting_team + "_" + imageSeasonId + ".jpg").placeholder(R.drawable.logo_signup).into(rightLogoImg);
                            } else if (rightLogoImg != null) {
                                leftFrame.setVisibility(View.VISIBLE);
                                rightFrame.setVisibility(View.VISIBLE);
                                Picasso.with(getActivity()).load(WebUrls.IMAGE_BASE_URL + visiting_team + "_" + imageSeasonId + ".jpg").placeholder(R.drawable.logo_signup).into(leftLogoImg);
                                Picasso.with(getActivity()).load(WebUrls.IMAGE_BASE_URL + home_team + "_" + imageSeasonId + ".jpg").placeholder(R.drawable.logo_signup).into(rightLogoImg);

                            }
                            /*
                            * *  homeHeaderWrapper = new HomeHeaderWrapper();
                             homeHeaderWrapper.setDate(date_time);
                             homeHeaderWrapper.setHomelogo(WebUrls.IMAGE_BASE_URL + home_team + "_" + season_id + ".jpg");
                             homeHeaderWrapper.setVisitedlogo(WebUrls.IMAGE_BASE_URL + visiting_team + "_" + season_id + ".jpg");
                             homeHeaderWrapper.setVenue(venueName);*/

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        startCountDown(diff);
                    } else {
                        // if (jo.has("message"))
                        //   ApplicationUtils.showMessage(getActivity(),ApplicationUtils.getkeyValue_Str(jo, "message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getNews();
            }

            @Override
            public void onTimeOutError() {
                try {
                    // ApplicationUtils.showMessage(getActivity(),getActivity().getResources().getString(R.string.error_internet2));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, false, 1);
    }

    private void cancelRefreshing() {
        if (swpLayoutHome != null)
            swpLayoutHome.setRefreshing(false);
    }

    private void startCountDown(long reminMilli) {
        timer = new CountDownTimer(reminMilli, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                long secs = millisUntilFinished / 1000 % 60;
                long mins = millisUntilFinished / (60 * 1000) % 60;
                long hours = millisUntilFinished / (60 * 60 * 1000) % 24;
                long days = millisUntilFinished / (24 * 60 * 60 * 1000);

                try {
                    if (daysTxt != null) {
                        String dayFormat = days < 10 ? "00" + days : "" + days;
                        daysTxt.setText("" + dayFormat);
                    }
                    if (hourTxt != null) {
                        String hourFormat = hours < 10 ? "0" + hours : "" + hours;
                        hourTxt.setText("" + hourFormat);
                    }
                    if (minTxt != null) {
                        String minHour = mins < 10 ? "0" + mins : "" + mins;
                        minTxt.setText("" + minHour);
                    }
                    if (secTxt != null) {
                        String minSec = secs < 10 ? "0" + secs : "" + secs;
                        secTxt.setText("" + minSec);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {

            }
        };
        timer.start();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timer != null)
            timer.cancel();
    }


}
