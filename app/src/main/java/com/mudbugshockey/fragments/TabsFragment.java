package com.mudbugshockey.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.mudbugshockey.R;
import com.mudbugshockey.activities.MainActivity;


public class TabsFragment extends Fragment implements TabHost.OnTabChangeListener {
    private static final String TAG = "FragmentTabs";
    public static final String HOMES = "homes";
    public static final String ROSTER = "roster";
    public static final String STANDINGS = "standings";
    public static final String SCHEDULE = "schedule";
    public static final String TICKETS = "tickets";

    private View mRoot;
    private TabHost mTabHost;
    private int mCurrentTab;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRoot = inflater.inflate(R.layout.fragments_tabs, container, false);
        mTabHost = (TabHost) mRoot.findViewById(android.R.id.tabhost);
        setupTabs();
        return mRoot;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        mTabHost.setOnTabChangedListener(this);
        mTabHost.setCurrentTab(mCurrentTab);
        // manually start loading stuff in the first tab
        FragmentManager fm = getFragmentManager();
        if (fm.findFragmentByTag(HOMES) == null) {
            fm.beginTransaction().replace(R.id.tab_1, new HomeFragment(), HOMES).commit();
        }
    }

    private void setupTabs() {
        mTabHost.setup(); // important!
        mTabHost.addTab(newTab(HOMES, R.drawable.home_tab_selector, R.id.tab_1, "HOME"));
        mTabHost.addTab(newTab(ROSTER, R.drawable.roster_tab_selector, R.id.tab_2, "ROSTER"));
        mTabHost.addTab(newTab(SCHEDULE, R.drawable.schedule_tab_selector, R.id.tab_4, "SCHEDULE"));
        mTabHost.addTab(newTab(STANDINGS, R.drawable.standings_tab_selector, R.id.tab_3, "STANDINGS"));
        mTabHost.addTab(newTab(TICKETS, R.drawable.buy_tab_selector, R.id.tab_5, "TICKETS"));
        FragmentManager fm = getFragmentManager();
        fm.beginTransaction()
                // .setCustomAnimations(R.anim.fadein, R.anim.fadeout)
                .replace(R.id.tab_1, new HomeFragment(), HOMES).commit();
    }

    private TabHost.TabSpec newTab(String tag, int labelId, int tabContentId, String text_) {
        Log.d(TAG, "buildTab(): tag=" + tag);

        View indicator = LayoutInflater.from(getActivity()).inflate(R.layout.tab,
                (ViewGroup) mRoot.findViewById(android.R.id.tabs), false);
        ((ImageView) indicator.findViewById(R.id.imgBtn)).setImageResource(labelId);
        ((TextView) indicator.findViewById(R.id.text)).setText(text_);
        TabHost.TabSpec tabSpec = mTabHost.newTabSpec(tag);
        tabSpec.setIndicator(indicator);
        tabSpec.setContent(tabContentId);
        return tabSpec;
    }

    @Override
    public void onTabChanged(String tabId) {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() >= 0) {
            fm.popBackStack();
        }
        Log.d(TAG, "onTabChanged(): tabId=" + tabId);
        switch (tabId) {
            case HOMES:
                ((MainActivity) getActivity()).mTitleTx.setText(getActivity().getResources().getString(R.string.shreveport_mudbugs));
                //kalu if (fm.findFragmentByTag(tabId) == null) {
                fm.beginTransaction()
                        // .setCustomAnimations(R.anim.fadein, R.anim.fadeout)
                        .replace(R.id.tab_1, new HomeFragment(), tabId).commit();
                //}
                mCurrentTab = 0;
                break;
            case ROSTER:
                ((MainActivity) getActivity()).mTitleTx.setText(getActivity().getResources().getString(R.string.roster));
                //if (fm.findFragmentByTag(tabId) == null) {
                fm.beginTransaction().replace(R.id.tab_2, new RosterFragment(), tabId).commit();
                //}
                mCurrentTab = 1;
                break;
            case SCHEDULE:
                ((MainActivity) getActivity()).mTitleTx.setText(getActivity().getResources().getString(R.string.schedules));
                //if (fm.findFragmentByTag(tabId) == null) {
                fm.beginTransaction().replace(R.id.tab_4, new ScheduleFragment(), tabId).commit();
                //}
                mCurrentTab = 2;
                break;
            case STANDINGS:
                ((MainActivity) getActivity()).mTitleTx.setText(getActivity().getResources().getString(R.string.standings));
                //if (fm.findFragmentByTag(tabId) == null) {
                fm.beginTransaction().replace(R.id.tab_3, new StandingsFragment(), tabId).commit();
                //}
                mCurrentTab = 3;
                break;

            case TICKETS:
                ((MainActivity) getActivity()).mTitleTx.setText(getActivity().getResources().getString(R.string.tickets));
                // if (fm.findFragmentByTag(tabId) == null) {
                fm.beginTransaction().replace(R.id.tab_5, new TicketsFragment(), tabId).commit();
                //}
                mCurrentTab = 4;
                break;

        }

    }
    public void getBackTicketsWeb(){
        TicketsFragment ticketsFrag = (TicketsFragment)getFragmentManager().findFragmentByTag(TICKETS);
        if (ticketsFrag != null && ticketsFrag.isVisible()) {
            // add your code here
            ticketsFrag.backWeb();
        }else{
            ((MainActivity)getActivity()).backPressAlert();
        }
    }


/**    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
super.onActivityResult(requestCode, resultCode, data);
if (requestCode == Utils.REQUEST_INTENT) {
if (resultCode == Utils.RESULT_INTENT) {
getActivity().finish();
System.exit(0);
android.os.Process.killProcess(android.os.Process.myPid());
}
}
}**/
}
