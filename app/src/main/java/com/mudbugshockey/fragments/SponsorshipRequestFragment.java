package com.mudbugshockey.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ScrollView;

import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.views.EditTextNunitoRegular;
import com.mudbugshockey.views.TextViewNunitoRegular;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SponsorshipRequestFragment extends Fragment {
    @Bind(R.id.compnynameEdt)
    EditTextNunitoRegular companyNameEdt;
    @Bind(R.id.nameEdt)
    EditTextNunitoRegular nameEdt;
    @Bind(R.id.emailEdt)
    EditTextNunitoRegular emailEdt;
    @Bind(R.id.mobilenumEdt)
    EditTextNunitoRegular mobileNumEdt;
    @Bind(R.id.messageEdt)
    EditTextNunitoRegular messageEdt;
    @Bind(R.id.submitTxt)
    TextViewNunitoRegular submitTxt;
    @Bind(R.id.sponsorshipScr)
    ScrollView sponsorshipScr;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View mRoot = inflater.inflate(R.layout.fragment_request_sponsorship, container, false);
        ButterKnife.bind(this, mRoot);
        //nameEdt.setText(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_FIRST_NAME) + " "
        //      + ApplicationUtils.getStringPrefs(ConstantUtils.KEY_LAST_NAME));
        //emailEdt.setText(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_EMAIL));
        //mobileNumEdt.setText(ApplicationUtils.getStringPrefs(ConstantUtils.KEY_PHONE_NUMBER));
        return mRoot;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.submitTxt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submitTxt:
                ApplicationUtils.getInstance().produceAnimation(submitTxt);
                ApplicationUtils.getInstance().hideSoftKeyBoard(((Activity) getActivity()));
                String[] arrCheck = data();
                if (checkInputAvailable(arrCheck)) {
                    Map<String, String> params = new HashMap<>();
                    params.put("logintoken", ApplicationUtils.getStringPrefs(ConstantUtils.KEY_USER_ID));
                    params.put("company", getEditedText(companyNameEdt));
                    params.put("name", getEditedText(nameEdt));
                    params.put("email", getEditedText(emailEdt));
                    params.put("mobile", getEditedText(mobileNumEdt));
                    params.put("message", getEditedText(messageEdt));
                    new WebTask(getActivity(), WebUrls.BASE_API_URL + "user/sponsorship", params, new WebCompleteTask() {
                        @Override
                        public void onComplete(String response, int taskcode) {
                            Log.e(ApplicationUtils.TAG, response);
                            try {
                                JSONObject jo = new JSONObject(response);
                                String status = ApplicationUtils.getkeyValue_Str(jo, "status");
                                messageEdt.setText("");
                                if (jo.has("message"))
                                    ApplicationUtils.showSnack(ApplicationUtils.getkeyValue_Str(jo, "message"), sponsorshipScr);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onTimeOutError() {
                            ApplicationUtils.showSnack(getActivity().getResources().getString(R.string.error_internet2), sponsorshipScr);
                        }
                    }, ConstantUtils.TASK_DEFAULT);
                }

                break;
        }
    }

    private String[] data() {
        String[] arr = new String[5];
        arr[0] = getEditedText(companyNameEdt);
        arr[1] = getEditedText(nameEdt);
        arr[2] = getEditedText(emailEdt);
        arr[3] = getEditedText(mobileNumEdt);
        arr[4] = getEditedText(messageEdt);
        return arr;
    }

    private String getEditedText(EditText edt) {
        String s = edt.getText().toString();
        if (TextUtils.isEmpty(s)) {
            s = "";
        }
        return s;
    }

    private boolean checkInputAvailable(String[] ar) {
        boolean retVal = true;
        String cName = ar[0];
        String name = ar[1];
        String email = ar[2];
        String phone = ar[3];

        if (TextUtils.isEmpty(phone) || !ApplicationUtils.getInstance().isValidPhoneNumber(phone) || phone.length() < 6 || phone.length() > 13) {
            ApplicationUtils.showSnack(getActivity().getResources().getString(R.string.phoneError), sponsorshipScr);
            retVal = false;
        }
        if (!ApplicationUtils.isValidEmail(email)) {
            ApplicationUtils.showSnack(SponsorshipRequestFragment.this.getResources().getString(R.string.emailError), sponsorshipScr);
            retVal = false;
        }
        if (TextUtils.isEmpty(name)) {
            ApplicationUtils.showSnack(getActivity().getResources().getString(R.string.nameEmptyError), sponsorshipScr);
            retVal = false;
        }
        if (TextUtils.isEmpty(cName)) {
            ApplicationUtils.showSnack(getActivity().getResources().getString(R.string.companyEmptyError), sponsorshipScr);
            retVal = false;
        }


        return retVal;

    }
}
