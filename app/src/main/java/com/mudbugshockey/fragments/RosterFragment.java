package com.mudbugshockey.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.adapter.RoosterRclAdapter;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.mudbugshockey.wrapper.RosterWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RosterFragment extends Fragment {

    @Bind(R.id.rclRooster)
    android.support.v7.widget.RecyclerView rclRooster;
    @Bind(R.id.swpLayoutRooster)
    android.support.v4.widget.SwipeRefreshLayout swpLayout;
    @Bind(R.id.llRooster)
    LinearLayout llRooster;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rosterView = inflater.inflate(R.layout.fragment_roster, container, false);
        ButterKnife.bind(this, rosterView);

        // Creating a layout Manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rclRooster.setLayoutManager(mLayoutManager);
        getPlayers();
        swpLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPlayers();
            }
        });
        return rosterView;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void getPlayers() {
        String param = "feed=modulekit&view=roster&key=bb92e4a0d0be791d&site_id=2&client_code=nahl&league_code=1&lang=en&fmt=json&team_id=24";
        Map<String, String> params = new HashMap<>();
        new WebTask(getActivity(), WebUrls.BASE_URL + param, params, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                cancelRefreshing();

                try {
                    JSONObject skObj;
                    List<RosterWrapper> arr = new ArrayList<>();
                    try {
                        JSONObject jo = new JSONObject(response);
                        skObj = jo.getJSONObject("SiteKit");

                        if (skObj == null) return;
                        JSONArray roosterArr = skObj.getJSONArray("Roster");
                        for (int i = 0; i < roosterArr.length(); i++) {
                            JSONObject joArr = roosterArr.getJSONObject(i);
                            if (joArr!=null) {
                                RosterWrapper rw = new RosterWrapper();
                                rw.setFirst_name(ApplicationUtils.getkeyValue_Str(joArr, "first_name"));
                                rw.setLast_name(ApplicationUtils.getkeyValue_Str(joArr, "last_name"));
                                rw.setTp_jersey_number(ApplicationUtils.getkeyValue_Str(joArr, "tp_jersey_number"));
                                rw.setPosition(ApplicationUtils.getkeyValue_Str(joArr, "position"));
                                rw.setPlayer_image(ApplicationUtils.getkeyValue_Str(joArr, "player_image"));
                                rw.setPlayerId(ApplicationUtils.getkeyValue_Str(joArr, "playerId"));
                                arr.add(rw);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    RoosterRclAdapter adapter = new RoosterRclAdapter(getActivity(),
                            arr, new RecycleClickListener() {
                        @Override
                        public void onRecycleClick(int position) {

                        }
                    });
                    rclRooster.setAdapter(adapter);// Setting the adapter to RecyclerView
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTimeOutError() {
                try {
                    cancelRefreshing();
                    ApplicationUtils.showSnack(getActivity().getResources().getString(R.string.error_internet2),
                            llRooster);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1);
    }
    private void cancelRefreshing(){
        if (swpLayout!=null)
            swpLayout.setRefreshing(false);
    }
}
