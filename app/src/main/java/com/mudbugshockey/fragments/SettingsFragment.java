package com.mudbugshockey.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mudbugshockey.R;
import com.mudbugshockey.activities.MainActivity;
import com.mudbugshockey.activities.NotificationActivity;
import com.mudbugshockey.activities.ProfileActivity;
import com.mudbugshockey.activities.TermsPolicyActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsFragment extends Fragment {


    @Bind(R.id.rlPrivacyPolicy)
    RelativeLayout rlPrivacyPolicy;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View mRoot = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, mRoot);

        return mRoot;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    @OnClick({R.id.rlAccountInfo, R.id.rlNotification, R.id.rlTermsOfService, R.id.rlPrivacyPolicy})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlAccountInfo:
                Intent in = new Intent(getActivity(), ProfileActivity.class);
                ((Activity) getActivity()).startActivityForResult(in, MainActivity.REQUEST_CODE_LOGOUT);
                break;
            case R.id.rlNotification:
                startActivity(new Intent(getActivity(), NotificationActivity.class));
                break;
            case R.id.rlTermsOfService:
                Intent policyIntent1 = new Intent(getActivity(), TermsPolicyActivity.class);
                policyIntent1.putExtra("tag", "Terms");
                startActivity(policyIntent1);
                break;
            case R.id.rlPrivacyPolicy:
                Intent policyIntent = new Intent(getActivity(), TermsPolicyActivity.class);
                policyIntent.putExtra("tag", "Privacy");
                startActivity(policyIntent);
                break;
        }
    }

}
