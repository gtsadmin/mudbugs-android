package com.mudbugshockey;


public class ConstantUtils {
    public static final String GOALIE = "Goalie";
    public static final String FORWARD = "Forward";
    public static final String DEFENSEMAN = "Defenseman";
    public static final String KEY_DEVICE_TOKEN = "key_device_token";
    public static final String KEY_IS_TOKEN_SAVED = "key_is_token_saved";
    public static final String KEY_FIRST_NAME = "key_first_name";
    public static final String KEY_LAST_NAME = "key_LAST_NAME";
    public static final String KEY_PHONE_NUMBER = "key_phone_number";
    public static final String KEY_PHOTO_URL = "key_photo_url";
    public static final String KEY_EMAIL = "key_email";
    public static final String KEY_USER_ID = "key_user_id";

    public static final String KEY_NEWS_FLAG = "key_news_flag";
    public static final String KEY_GAME_START = "key_game_start";
    public static final String KEY_GAME_RESULT = "key_game_result";
    public static final String KEY_GOALS = "key_goals";
    public static final int RESULT_FINISH_ACTIVITY = 15;
    public static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    public static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 102;
    public static final int REQUEST_CAMERA_ACCESS_PERMISSION = 103;
    public static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION_GET = 104;
    public static final int SEASON_ID = 38;
    public static final int TASK_DEFAULT = 45;
    public static final int TASK_UPDATE = 46;
    public static final int TASK_DELETE = 47;
    public static final int FLAG_LOGOUT = 41;
    public static final int FLAG_DELETE = 42;
    public static final int RESULT_UPDATE_PROFILE = 68;


    public static final int REQUEST_CODE_GALLERY_IMAGES = 22;
    public static final int REQUEST_CODE_CAMERA_PIC = 23;
    public static final int REQUEST_CODE_CROP_PIC = 27;
    public static final int MY_PERMISSION_CAMERA = 55;
    public static final int PERMISSION_READ_EXTERNAL_STORAGE = 56;
    public static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 57;
}
