package com.mudbugshockey;

public interface ImageDialogClickListener {
    void imageGalleryClick();
    void imageCameraClick();
}
