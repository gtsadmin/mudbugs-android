package com.mudbugshockey;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class PickImageDialog extends Dialog {
    Context ctx;
    ImageDialogClickListener listener;
    public PickImageDialog(Context context, ImageDialogClickListener listener1) {
        super(context, R.style.Theme_Custom);
        ctx = context;
        this.listener = listener1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_pick_image);
        ButterKnife.bind(this);


    }


    @OnClick({R.id.imgClose, R.id.txtCamera, R.id.txtGallery})
    public void onClick(View view) {
        dismiss();
        switch (view.getId()) {
            case R.id.imgClose:
                break;
            case R.id.txtCamera:
                listener.imageCameraClick();
                break;
            case R.id.txtGallery:
                listener.imageGalleryClick();
                break;
        }
    }
}
