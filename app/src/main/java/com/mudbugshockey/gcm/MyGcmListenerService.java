package com.mudbugshockey.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.mudbugshockey.R;
import com.mudbugshockey.activities.MainActivity;
import com.mudbugshockey.activities.SplashActivity;
import com.mudbugshockey.application.ApplicationUtils;

import java.util.Random;

/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
       // String bedge = data.getString("badge");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        showNotif(message);
    }


    private void showNotif(String message) {
        Intent intent;
        if (!ApplicationUtils.getInstance().isAppForeground()) {//when app in background
            intent = new Intent(MyGcmListenerService.this, SplashActivity.class);
        } else {
            intent = new Intent(MyGcmListenerService.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            //intent.putExtra("isFromPushClick", true);
/*            try {
                Intent bIntent = new Intent("custom-event-name");
                bIntent.putExtra("keyAction", "pushAction");
                LocalBroadcastManager.getInstance(MyGcmListenerService.this).sendBroadcast(bIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
*/
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);


        NotificationManager notificationManager = (NotificationManager) MyGcmListenerService.this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        // Notification notification = new Notification(icon, message, when);



        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(MyGcmListenerService.this)
                .setSmallIcon(R.mipmap.icon)
                .setContentTitle(MyGcmListenerService.this.getResources().getString(R.string.app_name))
                .setPriority(Notification.PRIORITY_HIGH)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message);

        PendingIntent pIntent = PendingIntent.getActivity(MyGcmListenerService.this, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);
        mBuilder.setContentIntent(pIntent);
        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        int min = 65;
        int max = 80;
        Random r = new Random();
        int i1 = r.nextInt(max - min + 1) + min;
        notificationManager.notify(i1, notification);
        PowerManager pm = (PowerManager) MyGcmListenerService.this.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if (!isScreenOn) {

            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK
                    | PowerManager.ACQUIRE_CAUSES_WAKEUP
                    | PowerManager.ON_AFTER_RELEASE, "MyLock");

            wl.acquire(10000);
            wl.release();
//kalu commented            WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");
//            wl_cpu.acquire(10000);
        }
    }
}
