package com.mudbugshockey;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotDialog extends Dialog {
    Context ctx;
    @Bind(R.id.emailForgotEdt)
    com.mudbugshockey.views.EditTextNunitoRegular emailForgotEdt;

    public ForgotDialog(Context context) {
        super(context, R.style.Theme_Custom);
        this.ctx = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_forgot);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.signinTxt})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.signinTxt:
                if (!ApplicationUtils.isValidEmail(emailForgotEdt.getText().toString()) || TextUtils.isEmpty(emailForgotEdt.getText().toString())) {
                    ApplicationUtils.showMessage(ctx, ctx.getResources().getString(R.string.emailError));
                    return;
                }
                hodeKeyboard();
                getForgot(emailForgotEdt.getText().toString());
                break;
        }
    }

    private void hodeKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(emailForgotEdt.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getForgot(String email) {
        Map<String, String> params = new HashMap<>();
        params.put("email", email);
        new WebTask(ctx, WebUrls.BASE_API_URL + "auth/forgot_password", params, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskCode) {
                dismiss();
                Log.e(ApplicationUtils.TAG, response);
                try {
                    JSONObject jo = new JSONObject(response);
                    String status = ApplicationUtils.getkeyValue_Str(jo, "status");
                    if (jo.has("message"))
                        ApplicationUtils.showMessage(ctx, ApplicationUtils.getkeyValue_Str(jo, "message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTimeOutError() {
                ApplicationUtils.spinnerStop();
                ApplicationUtils.showMessage(ctx, ctx.getResources().getString(R.string.error_internet2));
            }
        }, ConstantUtils.TASK_DEFAULT);
    }
}
