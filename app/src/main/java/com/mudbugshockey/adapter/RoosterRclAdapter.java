package com.mudbugshockey.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mudbugshockey.ConstantUtils;
import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.activities.PlayerProfileActivity;
import com.mudbugshockey.views.TextViewNunitoLight;
import com.mudbugshockey.views.TextViewNunitoRegular;
import com.mudbugshockey.wrapper.RosterWrapper;
import com.squareup.picasso.Picasso;

import java.util.List;


public class RoosterRclAdapter extends RecyclerView.Adapter<RoosterRclAdapter.ViewHolder> {
    private RecycleClickListener mListener;
    private Context mContext;
    private List<RosterWrapper> data;

    public RoosterRclAdapter(Context context,
                             List<RosterWrapper> arr, RecycleClickListener listener) {
        this.mListener = listener;
        this.mContext = context;
        this.data = arr;
    }

    public void notifyMe(List<RosterWrapper> arr) {
        this.data = arr;
        notifyDataSetChanged();
    }

    //Below first we ovverride the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_roster_list, parent, false); //Inflating the layout
        return new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view
        //inflate your layout and pass it to view holder
    }

    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RosterWrapper wrapper = data.get(position);
        holder.txtName.setText(wrapper.getFirst_name() + " " + wrapper.getLast_name());
        holder.txtRank.setText(wrapper.getTp_jersey_number());
        if (wrapper.getPosition().equals("F"))
            holder.txtPosition.setText(ConstantUtils.FORWARD);
        else if (wrapper.getPosition().equals("G"))
            holder.txtPosition.setText(ConstantUtils.GOALIE);
        else
            holder.txtPosition.setText((ConstantUtils.DEFENSEMAN));

        Picasso.with(mContext).load(wrapper.getPlayer_image()).placeholder(R.drawable.thumb_player)
                .error(R.drawable.thumb_player).into(holder.imageView);

        //holder.rowView.setTag(position);//set position as tag
        holder.mainBgRosterRl.setTag(position);
        holder.mainBgRosterRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer in = (Integer) v.getTag();
                mListener.onRecycleClick(in.intValue());
                Intent intent = new Intent(mContext, PlayerProfileActivity.class);
                intent.putExtra("playerId",data.get(in.intValue()).getPlayerId());
                ((Activity)mContext).startActivity(intent);
            }
        });
    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return data.size(); // the number of items in the list will be +1 the titles including the header view.
    }

    // Creating a ViewHolder which extends the RecyclerView View Holder
    // ViewHolder are used to to store the inflated views in order to recycle them

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        com.mudbugshockey.views.TextViewNunitoRegular txtName;
        com.mudbugshockey.views.TextViewNunitoLight txtPosition, txtRank;
        de.hdodenhof.circleimageview.CircleImageView imageView;
        RelativeLayout mainBgRosterRl;
        View rowView;

        public ViewHolder(View itemView, int ViewType) {                 // Creating ViewHolder Constructor with View and viewType As a parameter
            super(itemView);

            // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created
            txtName = (TextViewNunitoRegular) itemView.findViewById(R.id.nameTxt); // Creating TextView object with the id of textView from item_row.xml
            txtPosition = (TextViewNunitoLight) itemView.findViewById(R.id.positionlTxt);
            txtRank = (TextViewNunitoLight) itemView.findViewById(R.id.rankTxt);
            imageView = (de.hdodenhof.circleimageview.CircleImageView) itemView.findViewById(R.id.profileImage);
            mainBgRosterRl =(RelativeLayout)itemView.findViewById(R.id.mainBgRosterRl);
            this.rowView = itemView;
            //itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            Integer in = (Integer) v.getTag();
            mListener.onRecycleClick(in.intValue());
            Intent intent = new Intent(mContext, PlayerProfileActivity.class);
            intent.putExtra("playerId",data.get(in.intValue()).getPlayerId());
            ((Activity)mContext).startActivity(intent);
        }
    }

    // With the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        return 0;
    }


}
