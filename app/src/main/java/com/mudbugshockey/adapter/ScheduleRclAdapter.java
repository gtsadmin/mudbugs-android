package com.mudbugshockey.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.activities.BuyTicketActivity;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.views.TextViewNunitoLight;
import com.mudbugshockey.webutility.WebCompleteTask;
import com.mudbugshockey.webutility.WebTask;
import com.mudbugshockey.webutility.WebUrls;
import com.mudbugshockey.wrapper.ScheduleWrapper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class ScheduleRclAdapter extends RecyclerView.Adapter<ScheduleRclAdapter.ViewHolder> {
    private RecycleClickListener mListener;
    private Context mContext;
    private List<ScheduleWrapper> data;
    private LinearLayout llSchedule;
    Calendar cal = Calendar.getInstance();

    public ScheduleRclAdapter(Context context,
                              LinearLayout llSchedule_, List<ScheduleWrapper> arr, RecycleClickListener listener) {
        this.mListener = listener;
        this.mContext = context;
        this.data = arr;
        this.llSchedule = llSchedule_;
    }

    public void notifyMe(List<ScheduleWrapper> arr) {
        this.data = arr;
        notifyDataSetChanged();
    }

    //Below first we ovverride the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_schedule_list, parent, false); //Inflating the layout
        return new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view
        //inflate your layout and pass it to view holder
    }

    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ScheduleWrapper wrapper = data.get(position);
        holder.dateTxt.setText(wrapper.GameDate);
        String result;
        if (wrapper.GameStatusStringLong.contains("Final")) {

            //if (wrapper.home_team.equals("24")) {//home team - Shreveport Mudbugs
            result = wrapper.VisitorGoals + ":" + wrapper.HomeGoals;
            if (Integer.parseInt(wrapper.HomeGoals) > Integer.parseInt(wrapper.VisitorGoals)) {
                holder.statusMeTxt.setText("L");
                holder.statusOtherTxt.setText("W");
            } else if (Integer.parseInt(wrapper.HomeGoals) < Integer.parseInt(wrapper.VisitorGoals)) {
                holder.statusMeTxt.setText("W");
                holder.statusOtherTxt.setText("L");
            } else {
                holder.statusMeTxt.setText("");
                holder.statusOtherTxt.setText("");
            }

            /*} else {
                result = wrapper.visiting_goal_count + ":" + wrapper.home_goal_count;
                if (Integer.parseInt(wrapper.visiting_goal_count) > Integer.parseInt(wrapper.home_goal_count)) {
                    holder.statusMeTxt.setText("W");
                    holder.statusOtherTxt.setText("L");
                } else if (Integer.parseInt(wrapper.visiting_goal_count) < Integer.parseInt(wrapper.home_goal_count)) {
                    holder.statusMeTxt.setText("L");
                    holder.statusOtherTxt.setText("W");
                } else {
                    holder.statusMeTxt.setText("");
                    holder.statusOtherTxt.setText("");
                }

           }*/

            holder.mainBgTransRl.setVisibility(View.GONE);
            holder.mainBgRl.setVisibility(View.VISIBLE);
            holder.mainBgRl.setBackgroundColor(ContextCompat.getColor(mContext, R.color.grey_bg_semiTran));


        } else {
            holder.mainBgTransRl.setVisibility(View.VISIBLE);
            holder.mainBgRl.setVisibility(View.GONE);
            holder.statusMeTxt.setText("");
            holder.statusOtherTxt.setText("");
            result = wrapper.GameStatusStringLong;
            holder.mainBgRl.setBackgroundColor(ContextCompat.getColor(mContext, R.color.transparent));
        }
        holder.resultTxt.setText(result.toUpperCase());
        holder.venueTxt.setText(wrapper.venue_name);
        //if (wrapper.home_team.equals("24")) {//home team - Shreveport Mudbugs
        Picasso.with(mContext).load(WebUrls.IMAGE_BASE_URL + wrapper.HomeID + "_" + ApplicationUtils.getInstance().getImage_seasonId() + ".jpg").placeholder(R.mipmap.icon)
                .error(R.mipmap.icon).into(holder.rightLogoImg);
        Picasso.with(mContext).load(WebUrls.IMAGE_BASE_URL + wrapper.VisitorID + "_" + ApplicationUtils.getInstance().getImage_seasonId() + ".jpg").placeholder(R.mipmap.icon)
                .error(R.mipmap.icon).into(holder.leftLogoImg);
        /*} else {//visiting team
            Picasso.with(mContext).load(WebUrls.IMAGE_BASE_URL + wrapper.home_team + "_" + wrapper.season_id + ".jpg").placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher).into(holder.rightLogoImg);
            Picasso.with(mContext).load(WebUrls.IMAGE_BASE_URL + wrapper.visiting_team + "_" + wrapper.season_id + ".jpg").placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher).into(holder.leftLogoImg);
        }*/


        holder.mainBgTransRl.setTag(position);
        holder.mainBgTransRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer in = (Integer) v.getTag();
                mListener.onRecycleClick(in.intValue());

                if (data.get(in.intValue()).GameStatusStringLong.contains("Final")) {
                    //((Activity) mContext).startActivity(new Intent(mContext, GameResultActivity.class));
                } else {
                    try {
                        String datePlayed = data.get(in.intValue()).Date;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        cal.setTime(sdf.parse(datePlayed));
                        cal.add(Calendar.DATE, 1);
                        String throuDate = sdf.format(cal.getTime());
                        onItemClick(datePlayed, throuDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        // holder.rowView.setTag(position);//set position as tag

    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return data.size(); // the number of items in the list will be +1 the titles including the header view.
    }

    // Creating a ViewHolder which extends the RecyclerView View Holder
    // ViewHolder are used to to store the inflated views in order to recycle them

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextViewNunitoLight dateTxt, timeTxt, resultTxt, venueTxt, statusMeTxt, statusOtherTxt;
        ImageView leftLogoImg, rightLogoImg;
        RelativeLayout mainBgRl, mainBgTransRl;
        View rowView;

        public ViewHolder(View itemView, int ViewType) {
            // Creating ViewHolder Constructor with View and viewType As a parameter
            super(itemView);

            // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created
            dateTxt = (TextViewNunitoLight) itemView.findViewById(R.id.dateTxt); // Creating TextView object with the id of textView from item_row.xml
            timeTxt = (TextViewNunitoLight) itemView.findViewById(R.id.timeTxt);
            resultTxt = (TextViewNunitoLight) itemView.findViewById(R.id.resultTxt);
            venueTxt = (TextViewNunitoLight) itemView.findViewById(R.id.venueTxt);

            statusMeTxt = (TextViewNunitoLight) itemView.findViewById(R.id.statusMeTxt);
            statusOtherTxt = (TextViewNunitoLight) itemView.findViewById(R.id.statusOtherTxt);

            leftLogoImg = (ImageView) itemView.findViewById(R.id.leftLogoImg);
            rightLogoImg = (ImageView) itemView.findViewById(R.id.rightLogoImg);
            mainBgRl = (RelativeLayout) itemView.findViewById(R.id.mainBgRl);
            mainBgTransRl = (RelativeLayout) itemView.findViewById(R.id.mainBgTransRl);
            this.rowView = itemView;
            //itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            Integer in = (Integer) v.getTag();
            mListener.onRecycleClick(in.intValue());

            if (data.get(in.intValue()).GameStatusStringLong.contains("Final")) {
                //((Activity) mContext).startActivity(new Intent(mContext, GameResultActivity.class));
            } else {
                try {
                    String datePlayed = data.get(in.intValue()).Date;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    cal.setTime(sdf.parse(datePlayed));
                    cal.add(Calendar.DATE, 1);
                    String throuDate = sdf.format(cal.getTime());
                    onItemClick(datePlayed, throuDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // With the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    /////call on item click
    //http://www.ticketfly.com/api/events/list.json?orgId=1&fromDate=2017-03-30&thruDate=2017-04-01&q=Shreveport+Mudbugs+vs.+Corpus+Christi+IceRays&fields=ticketPurchaseUrl
    private void onItemClick(String fromDate, String throuDate) {
        String param = "list.json?orgId=1&fromDate=" + fromDate + "&thruDate=" + throuDate + "&q=Shreveport+Mudbugs+vs.+Corpus+Christi+IceRays&fields=ticketPurchaseUrl";
        Map<String, String> params = new HashMap<>();
        new WebTask(mContext, WebUrls.TICKET_FLY_URL + param, params, new WebCompleteTask() {
            @Override
            public void onComplete(String response, int taskcode) {
                try {
                    JSONObject jo = new JSONObject(response);
                    if (jo.getString("status").equals("ok")) {
                        String ticketUrl = "";
                        JSONArray jaEvents = jo.getJSONArray("events");
                        for (int i = 0; i < jaEvents.length(); i++) {
                            JSONObject joAr = jaEvents.getJSONObject(i);
                            ticketUrl = ApplicationUtils.getkeyValue_Str(joAr, "ticketPurchaseUrl");
                        }
                        if (!TextUtils.isEmpty(ticketUrl)) {
                            Intent merchIntent = new Intent(mContext, BuyTicketActivity.class);
                            merchIntent.putExtra("url", ticketUrl);
                            merchIntent.putExtra("title", mContext.getResources().getString(R.string.tickets));
                            mContext.startActivity(merchIntent);
                        } else {
                            // ApplicationUtils.showSnack(mContext.getResources().getString(R.string.error_ticket_not),
                            //     llSchedule);
                            ApplicationUtils.simplePop(mContext, mContext.getResources().getString(R.string.error_ticket_not));

                        }
                    } else {// data not available
                        ApplicationUtils.showSnack(mContext.getResources().getString(R.string.data_not_available),
                                llSchedule);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTimeOutError() {
                try {
                    ApplicationUtils.showSnack(mContext.getResources().getString(R.string.error_internet2),
                            llSchedule);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1);
    }
}
