package com.mudbugshockey.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    final int PAGE_COUNT = 2;

    Fragment frag[];
    String[] names;
    public ViewPagerAdapter(FragmentManager fm, Fragment[] fragArr, String[]fNames) {
        super(fm);
        this.frag = fragArr;
        this.names = fNames;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return frag[position];
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return names[position];
    }

}
