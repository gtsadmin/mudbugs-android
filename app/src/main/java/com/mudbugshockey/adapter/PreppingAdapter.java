package com.mudbugshockey.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mudbugshockey.R;
import com.mudbugshockey.views.SectionedBaseAdapter;
import com.mudbugshockey.wrapper.Row;
import com.mudbugshockey.wrapper.StandingsWrapper;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PreppingAdapter extends SectionedBaseAdapter {

    private LayoutInflater inflater;
    private List<StandingsWrapper> allData;
    private Context mContext;

    public PreppingAdapter(Context context,
                           List<StandingsWrapper> data) {
        this.allData = data;
        this.mContext = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Row getItem(int section, int position) {
        return allData.get(section).data.get(position);
    }


    @Override
    public long getItemId(int section, int position) {
        return 0;
    }

    @Override
    public int getSectionCount() {
        return allData.size();
        //return 5;
    }

    @Override
    public int getCountForSection(int section) {
        return allData.get(section).data.size();
        //return 1;
    }

    @Override
    public View getItemView(int section, int position, View convertView, ViewGroup parent) {
        ViewHolderItem holder;
        if (convertView != null) {
            holder = (ViewHolderItem) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.list_prepping_section, parent, false);
            holder = new ViewHolderItem(convertView);
            convertView.setTag(holder);
        }
        Row wprItem = getItem(section, position);
        holder.divisionValueTxt.setText(wprItem.getName());
        holder.gpValueTxt.setText(wprItem.getGames_played());
        holder.wValueTxt.setText(wprItem.getWins());
        holder.lValueTxt.setText(wprItem.getLosses());
        holder.olValueTxt.setText(wprItem.getOt_losses());
        holder.solValueTxt.setText(wprItem.getShootout_losses());
        holder.ptsValueTxt.setText(wprItem.getPoints());
        // JobHIstoryDetailWrapper itemShift = getItem(section, position);

        return convertView;
    }

    @Override
    public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
        ViewHolderHeader holder;
        if (convertView != null) {
            holder = (ViewHolderHeader) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.list_prepping_header, parent, false);
            holder = new ViewHolderHeader(convertView);
            convertView.setTag(holder);
        }
       String sectionName = allData.get(section).getTitleName();

        if (section != 5) {
            holder.divisionTitleTxt.setText(sectionName);
        }

        return convertView;
    }


    static class ViewHolderHeader {
        @Bind(R.id.divisionTitleTxt)
        com.mudbugshockey.views.TextViewNunitoRegular divisionTitleTxt;
        ViewHolderHeader(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class ViewHolderItem {
        @Bind(R.id.divisionValueTxt)
        com.mudbugshockey.views.TextViewNunitoLight divisionValueTxt;
        @Bind(R.id.gpValueTxt)
        com.mudbugshockey.views.TextViewNunitoLight gpValueTxt;
        @Bind(R.id.wValueTxt)
        com.mudbugshockey.views.TextViewNunitoLight wValueTxt;
        @Bind(R.id.lValueTxt)
        com.mudbugshockey.views.TextViewNunitoLight lValueTxt;
        @Bind(R.id.olValueTxt)
        com.mudbugshockey.views.TextViewNunitoLight olValueTxt;
        @Bind(R.id.solValueTxt)
        com.mudbugshockey.views.TextViewNunitoLight solValueTxt;
        @Bind(R.id.ptsValueTxt)
        com.mudbugshockey.views.TextViewNunitoLight ptsValueTxt;


        ViewHolderItem(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
