package com.mudbugshockey.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.activities.BuyTicketActivity;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.views.TextViewNunitoLight;
import com.mudbugshockey.wrapper.EventsWrapper;

import java.util.List;


public class TicketFlyRclAdapter extends RecyclerView.Adapter<TicketFlyRclAdapter.ViewHolder> {
    private RecycleClickListener mListener;
    private Context mContext;
    private List<EventsWrapper> data;

    public TicketFlyRclAdapter(Context context,
                               List<EventsWrapper> arr, RecycleClickListener listener) {
        this.mListener = listener;
        this.mContext = context;
        this.data = arr;
    }

    public void notifyMe(List<EventsWrapper> arr) {
        this.data = arr;
        notifyDataSetChanged();
    }

    //Below first we ovverride the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ticket_fly, parent, false); //Inflating the layout
        return new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view
        //inflate your layout and pass it to view holder
    }

    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EventsWrapper wrapper = data.get(position);
        if (wrapper.eventStatus.equals("Buy")) {
            holder.ticketButton.setBackgroundResource(R.drawable.shape_button);
            holder.ticketButton.setText(mContext.getResources().getString(R.string.getTickets));
        } else {
            holder.ticketButton.setBackgroundResource(R.drawable.shape_button_grey);
            holder.ticketButton.setText(mContext.getResources().getString(R.string.tickets_at_door));
        }
        holder.ticketButton.setTag(wrapper);
        holder.ticketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApplicationUtils.getInstance().produceAnimation(v);
                EventsWrapper wrapper = (EventsWrapper) v.getTag();
                if (wrapper.eventStatus.equals("Buy")) {
                    Intent merchIntent = new Intent(mContext, BuyTicketActivity.class);
                    merchIntent.putExtra("url", wrapper.ticketPurchaseUrl);
                    merchIntent.putExtra("title", mContext.getResources().getString(R.string.getTickets));
                    mContext.startActivity(merchIntent);
                }
            }
        });
        holder.nameTxt.setText(wrapper.name);
        holder.rateRangeTxt.setText(wrapper.ticketPrice);
        holder.showTimeTxt.setText("Show: "+ApplicationUtils.getInstance()
                .formatDate(wrapper.startDate, "yyyy-MM-dd HH:mm:ss", "MMM,dd yyyy hh:mm a"));
      //  Picasso.with(mContext).load(wrapper.image.square.path).placeholder(R.mipmap.ic_launcher)
          //      .error(R.mipmap.ic_launcher).into(holder.thumb);
        holder.rowView.setTag(position);//set position as tag

    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return data.size(); // the number of items in the list will be +1 the titles including the header view.
    }

    // Creating a ViewHolder which extends the RecyclerView View Holder
    // ViewHolder are used to to store the inflated views in order to recycle them

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextViewNunitoLight nameTxt, rateRangeTxt, showTimeTxt;
       // ImageView thumb;
        Button ticketButton;
        View rowView;

        public ViewHolder(View itemView, int ViewType) {
            super(itemView);

            // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created
            nameTxt = (TextViewNunitoLight)itemView.findViewById(R.id.nameTxt);
            rateRangeTxt = (TextViewNunitoLight) itemView.findViewById(R.id.rateRangeTxt); // Creating TextView object with the id of textView from item_row.xml
            showTimeTxt = (TextViewNunitoLight) itemView.findViewById(R.id.showTimeTxt);
            ticketButton = (Button) itemView.findViewById(R.id.ticketButton);
            //thumb = (ImageView) itemView.findViewById(R.id.thumb);
            this.rowView = itemView;
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            // Integer in = (Integer) v.getTag();
            // mListener.onRecycleClick(in.intValue());

          /*  if (data.get(in.intValue()).final_.equals("1")) {
                //((Activity) mContext).startActivity(new Intent(mContext, GameResultActivity.class));
            } else {
                Intent buyticket = new Intent(mContext, BuyTicketActivity.class);
                buyticket.putExtra("url", WebUrls.BUY_TICKET_URL);
                buyticket.putExtra("title", mContext.getResources().getString(R.string.season_tickets));
                ((Activity) mContext).startActivity(buyticket);
            }*/
        }
    }

    // With the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        return 0;
    }


}
