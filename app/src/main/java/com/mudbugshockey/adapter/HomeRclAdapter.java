package com.mudbugshockey.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.wrapper.NewsWrapper;
import com.squareup.picasso.Picasso;

import java.util.List;


public class HomeRclAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private RecycleClickListener mListener;
    private Context mContext;
    private List<NewsWrapper> data;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    //HomeHeaderWrapper homeHeaderWrapper;

    public HomeRclAdapter(Context context,
                          List<NewsWrapper> arr, RecycleClickListener listener) {//HomeHeaderWrapper wrapper
        this.mListener = listener;
        this.mContext = context;
        this.data = arr;
        //  this.homeHeaderWrapper = wrapper;
    }

    public void notifyMe(List<NewsWrapper> arr) {
        this.data = arr;
        notifyDataSetChanged();
    }


    //Below first we ovverride the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_header, parent, false);
            return new VHHeader(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_list, parent, false); //Inflating the layout
            return new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view
        }**/

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_list, parent, false); //Inflating the layout
        return new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view

    }

    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

      /*  if (holder instanceof VHHeader) {

            VHHeader VHitem = (VHHeader) holder;

            if (homeHeaderWrapper != null) {

                Picasso.with(mContext).load(homeHeaderWrapper.getVisitedlogo()).into(VHitem.leftLogoImg);
                Picasso.with(mContext).load(homeHeaderWrapper.getHomelogo()).into(VHitem.rightLogoImg);


                VHitem.dateTxt.setText(ApplicationUtils.getInstance()
                        .formatDate(homeHeaderWrapper.getDate(), "yyyy-MM-dd HH:mm:ss", "EEE, MMM dd, yyyy hh:mm a"));
                VHitem.venueTxt.setText(homeHeaderWrapper.getVenue());
            }


        } else if (holder instanceof ViewHolder) {*/
        NewsWrapper wrapper = data.get(position);

        ViewHolder VHitem = (ViewHolder) holder;
        if (wrapper.type.equals("photo")) {
            VHitem.paraLl.setVisibility(View.VISIBLE);
            VHitem.videoViewRl.setVisibility(View.GONE);
            VHitem.imageFrame.setVisibility(View.VISIBLE);
            try {
                VHitem.paraTxt.setText(wrapper.message);
                String url = wrapper.full_picture.replaceAll("\"", "");
                Picasso.with(mContext).load(url).placeholder(R.drawable.thumb_article)
                        .error(R.drawable.thumb_article).into(VHitem.paraImg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (wrapper.type.equals("status")) {
            VHitem.paraLl.setVisibility(View.VISIBLE);
            VHitem.videoViewRl.setVisibility(View.GONE);
            VHitem.imageFrame.setVisibility(View.GONE);
            VHitem.paraTxt.setText(wrapper.message);
        } else if (wrapper.type.equals("video")){
            VHitem.paraLl.setVisibility(View.GONE);
            VHitem.videoViewRl.setVisibility(View.VISIBLE);
            String url = wrapper.full_picture.replaceAll("\"", "");
            Picasso.with(mContext).load(url).placeholder(R.drawable.thumb_vid)
                    .error(R.drawable.thumb_vid).into(VHitem.videoViewDummy);


        }else{

        }
        VHitem.videoViewDummy.setTag(wrapper);
        VHitem.videoViewDummy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewsWrapper wrap = (NewsWrapper)v.getTag();
                Intent videoIntent =new Intent(Intent.ACTION_VIEW);
                videoIntent.setDataAndType(Uri.parse(wrap.source), "video/*");
                mContext.startActivity(videoIntent);

            }
        });

        VHitem.rowView.setTag(position);//set position as tag
        //}

    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return data.size(); // the number of items in the list will be +1 the titles including the header view.
    }

    // Creating a ViewHolder which extends the RecyclerView View Holder
    // ViewHolder are used to to store the inflated views in order to recycle them

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // RelativeLayout matchView, videoView;
        com.mudbugshockey.views.TextViewNunitoLight paraTxt;
        ImageView paraImg, videoViewDummy;
        RelativeLayout videoViewRl;
        LinearLayout paraLl;
        FrameLayout imageFrame;
        View rowView;

        public ViewHolder(View itemView, int ViewType) {// Creating ViewHolder Constructor with View
            // and viewType As a parameter
            super(itemView);

            // Here we set the appropriate view in accordance with the the view type as passed when
            // the holder object is created
            paraImg = (ImageView) itemView.findViewById(R.id.paraImg);
            videoViewDummy = (ImageView) itemView.findViewById(R.id.videoViewDummy);
            paraTxt = (com.mudbugshockey.views.TextViewNunitoLight) itemView.findViewById(R.id.paraTxt);
            videoViewRl = (RelativeLayout) itemView.findViewById(R.id.videoViewRl);
            paraLl = (LinearLayout) itemView.findViewById(R.id.paraLl);
            imageFrame = (FrameLayout) itemView.findViewById(R.id.imageFrame);
            this.rowView = itemView;
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            Integer in = (Integer) v.getTag();
            mListener.onRecycleClick(in.intValue());
        }
    }

    class VHHeader extends RecyclerView.ViewHolder {

        com.mudbugshockey.views.TextViewNunitoLight dateTxt, timeTxt, venueTxt;
        ImageView leftLogoImg, rightLogoImg;

        public VHHeader(View itemView) {
            super(itemView);
            leftLogoImg = (ImageView) itemView.findViewById(R.id.leftLogoImg);
            rightLogoImg = (ImageView) itemView.findViewById(R.id.rightLogoImg);
            dateTxt = (com.mudbugshockey.views.TextViewNunitoLight) itemView.findViewById(R.id.dateTxt);
            venueTxt = (com.mudbugshockey.views.TextViewNunitoLight) itemView.findViewById(R.id.venueTxt);
        }
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }


    // With the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        //if (isPositionHeader(position))
        // return TYPE_HEADER;
        return TYPE_ITEM;
    }


}
