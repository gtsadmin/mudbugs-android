package com.mudbugshockey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mudbugshockey.R;
import com.mudbugshockey.RecycleClickListener;
import com.mudbugshockey.application.ApplicationUtils;
import com.mudbugshockey.views.TextViewNunitoLight;
import com.mudbugshockey.wrapper.NotificationWrapper;

import java.util.List;


public class NotificationRclAdapter extends RecyclerView.Adapter<NotificationRclAdapter.ViewHolder> {
    private RecycleClickListener mListener;
    private Context mContext;
    private List<NotificationWrapper> data;

    public NotificationRclAdapter(Context context,
                                  List<NotificationWrapper> arr, RecycleClickListener listener) {
        this.mListener = listener;
        this.mContext = context;
        this.data = arr;
    }

    public void notifyMe(List<NotificationWrapper> arr) {
        this.data = arr;
        notifyDataSetChanged();
    }

    //Below first we ovverride the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notifications, parent, false); //Inflating the layout
        return new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view
        //inflate your layout and pass it to view holder
    }

    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NotificationWrapper wrapper = data.get(position);
        holder.dateTxt.setText(ApplicationUtils.getInstance().formatDate(wrapper.create_date,"yyyy-MM-dd HH:mm:ss","yyyy/MM/dd hh:mm a"));
        holder.contentTxt.setText(wrapper.message);
        //holder.rowView.setTag(position);//set position as tag
        holder.mainBgNotificationsRl.setTag(position);
        holder.mainBgNotificationsRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Integer in = (Integer) v.getTag();
               // mListener.onRecycleClick(in.intValue());
            }
        });
    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return data.size(); // the number of items in the list will be +1 the titles including the header view.
    }

    // Creating a ViewHolder which extends the RecyclerView View Holder
    // ViewHolder are used to to store the inflated views in order to recycle them

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextViewNunitoLight dateTxt, contentTxt;
        RelativeLayout mainBgNotificationsRl;
        View rowView;

        public ViewHolder(View itemView, int ViewType) {                 // Creating ViewHolder Constructor with View and viewType As a parameter
            super(itemView);

            // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created
            dateTxt = (TextViewNunitoLight) itemView.findViewById(R.id.dateTxt);
            contentTxt = (TextViewNunitoLight) itemView.findViewById(R.id.contentTxt);
            mainBgNotificationsRl =(RelativeLayout)itemView.findViewById(R.id.mainBgNotificationsRl);
            this.rowView = itemView;
            //itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            //Integer in = (Integer) v.getTag();
            //mListener.onRecycleClick(in.intValue());
        }
    }

    // With the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        return 0;
    }


}
